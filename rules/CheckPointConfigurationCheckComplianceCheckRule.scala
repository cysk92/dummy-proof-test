package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{MultiSnapshotExtractScalarExpression, SelectSnapshotsExpression, SelectTagsExpression, SnapshotExpression}
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.ruleengine.expressions.utility._
import com.indeni.ruleengine.expressions.{Expression, conditions}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.sensor.models.automationpolicy.AutomationPolicyItemAlertSetting
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  * Created by amir on 19/02/2017.
  */
case class CheckPointConfigurationCheckComplianceCheckRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  import CheckPointConfigurationCheckComplianceCheckRule._

  private[library] val linesParameterName = "config_lines_to_look_for"
  private val linesParameter = new ParameterDefinition(
    linesParameterName,
    "",
    "Configuration Lines Needed (multi-line)",
    "List the configuration commands that need to show in the output of \"show configuration\".",
    UIType.MULTILINE_TEXT,
    "")


  override def expressionTree: StatusTreeExpression = {
    val linesExpected = DynamicParameterExpression.withConstantDefault(context.parametersDao, metadata.name, linesParameter.getName, ConfigurationSetId, Seq[String]()).withLazy
    val snapshotKey = "configuration-content"
    val missings = SeqDiffWithoutOrderExpression[String](
      MultiSnapshotExtractScalarExpression(SnapshotExpression(snapshotKey).asMulti().mostRecent(), "line"),
      linesExpected
    ).missings.withLazy

    val missingLineHeadline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = scope.getVisible(CheckPointConfigurationTag).get.toString

      override def args: Set[Expression[_]] = Set()
    }

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The snapshot we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set(snapshotKey)).multi(),

        // The condition which, if true, we have an issue. Checked against the snapshots we've collected
        missings.nonEmpty,

        multiInformers =
          Set(
            MultiIssueInformer(
              missingLineHeadline,
              EMPTY_STRING,
              "Missing lines"
            ).iterateOver(collection = missings,
              scopeKey = CheckPointConfigurationTag,
              condition = conditions.True)
          )

        // Details of the alert itself
      ).withRootInfo(
          getHeadline(),
          ConstantExpression("Some configuration lines that should be defined are not:\n" +
            "indeni has found that some lines are missing. These are listed below."),
          ConstantExpression("Modify the device's configuration as required.")
      ).asCondition()
    ).withoutInfo()
  }

  /**
    * @return The rule's metadata.
    */
  override def metadata: RuleMetadata =
    RuleMetadata(
      "checkpoint_config_verification",
      "Compliance Check: Configuration Verification (Check Point Gaia)",
      "indeni can verify that certain lines appear in the \"show configuration\" output for Check Point Gaia.",
      AlertSeverity.ERROR,
      AutomationPolicyItemAlertSetting.NEVER,
      linesParameter
    )
}



object CheckPointConfigurationCheckComplianceCheckRule {
  val CheckPointConfigurationTag = "CPConfigurationTag"
}
