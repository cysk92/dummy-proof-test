package com.indeni.server.rules.library

import com.indeni.data.conditions.{TagsStoreCondition, True}
import com.indeni.ruleengine.expressions.conditions.{And, GreaterThanOrEqual}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.library.ThresholdDirection.ThresholdDirection
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

class NearingCapacityTemplateRule(context: RuleContext, ruleName: String, ruleFriendlyName: String, ruleDescription: String,
                                       severity: AlertSeverity = AlertSeverity.ERROR,
                                       usageMetricName: String, limitMetricName: String = null, threshold: Double,
                                       minimumValueToAlert: Double = 0, metaCondition: TagsStoreCondition = True,
                                       alertDescriptionFormat: String, baseRemediationText: String, thresholdDirection: ThresholdDirection = ThresholdDirection.ABOVE)
                                      (vendorToRemediationText: (String, String)*) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Store_use"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Usage",
    "indeni will evaluate the current utilization vs the limit and alert if the percentage of usage crosses this number.",
    UIType.DOUBLE,
    threshold)

  override val metadata: RuleMetadata = RuleMetadata(ruleName, ruleFriendlyName, ruleDescription, severity, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {

    val inUseValue = TimeSeriesExpression[Double](usageMetricName).last
    val thresholdValue = getParameterDouble(context, highThresholdParameter)
    val limitValue = if (null != limitMetricName) TimeSeriesExpression[Double](limitMetricName).last else ConstantExpression(Some(100.0))

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, if (null != limitMetricName) Set(usageMetricName, limitMetricName) else Set(usageMetricName), denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            And(
              generateCompareCondition(
                thresholdDirection,
                inUseValue,
                TimesExpression[Double](limitValue, DivExpression[Double](getParameterDouble(context, highThresholdParameter), ConstantExpression[Option[Double]](Some(100.0))))),
              GreaterThanOrEqual(inUseValue, ConstantExpression(Some(minimumValueToAlert))))

            // The Alert Item to add for this specific item
          ).withRootInfo(
              getHeadline(),
              scopableStringFormatExpression(alertDescriptionFormat, inUseValue, limitValue),
              ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText:_*)
        ).asCondition()
    ).withoutInfo()
  }
}
