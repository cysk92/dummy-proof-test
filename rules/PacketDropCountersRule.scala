package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions._
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.{ScopableExpression, ScopeValueExpression}
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class PacketDropCountersRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_packet_drops", "All Devices: Packet drop counters increasing",
    "indeni will track packet drop counters and alert if any important counters are incrementing.", AlertSeverity.ERROR)

  /**
    * When alerting about specific kernel tables, we will look for the first match in this key-value sequence and use it to
    * provide further insight regarding the table. NOTE: ORDER IS IMPORTANT.
    */
  val counterDetailsMap = Seq (
    "(?i)flow_tunnel_decap_err".r -> "Review https://live.paloaltonetworks.com/t5/Learning-Articles/IPSec-Tunnel-is-Up-but-Packet-is-Getting-Dropped-with-Wrong-SPI/ta-p/61918 for more information.",
    "(?i)flow_tunnel_ipsec_wrong_spi".r -> "Review https://live.paloaltonetworks.com/t5/Learning-Articles/IPSec-Tunnel-is-Up-but-Packet-is-Getting-Dropped-with-Wrong-SPI/ta-p/61918 for more information.",
    "(?i)flow_tunnel_natt_nomatch".r -> "Review https://live.paloaltonetworks.com/t5/Learning-Articles/IPSec-Tunnel-is-Up-but-Packet-is-Getting-Dropped-with-Wrong-SPI/ta-p/61918 for more information.",

    // Catch-all
    ".*".r -> "Please consult with your technical support provider about this counter."
  )

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("packet-drop-counter").last
    val counterName = ScopeValueExpression("name").visible().asString()

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name", "description"), withTagsCondition("packet-drop-counter")),

        StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("packet-drop-counter"), denseOnly = false),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              And(
                Not(Contains(ConstantExpression(Seq("flow_tcp_non_syn_drop", "flow_fwd_l3_bcast_drop", "flow_host_service_deny", "flow_ipv6_disabled", "flow_rcv_dot1q_tag_err", "flow_parse_l4_tcpsynfin", "flow_parse_l4_tcpfin", "flow_fwd_l3_mcast_drop", "")), counterName)),
                GreaterThanOrEqual(
                  actualValue,
                  ConstantExpression(Some(1.0))))

              // The Alert Item to add for this specific item
        ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                new ScopableExpression[String] {

                  override protected def evalWithScope(time: Long, scope: Scope): String = {
                    val name = scope.getVisible("name").get.toString
                    val description = scope.getVisible("name").get.toString

                    val counterDetails = counterDetailsMap.collectFirst {
                      case item if (!item._1.findFirstMatchIn(name).isEmpty) => item._2
                    }.get
                    description + " is increasing at a rate of " + actualValue.eval(time).get.toInt + " per second. " + counterDetails
                  }

                  override def args: Set[Expression[_]] = Set()
                },
                title = "Affected Counters"
            ).asCondition()
        ).withoutInfo().asCondition()


      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("Some devices track the number of packets being dropped for various reasons. The current packet drop counters which are indicating dropped packets are listed below."),
        ConstantExpression("Contact your technical support provider.")
    )
  }
}
