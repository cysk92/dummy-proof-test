package com.indeni.server.rules.library

import com.indeni.data.conditions._
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.server.params.{ParameterDefinition, ParameterValue}
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}


case class NicRxDropsRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "High_Threshold_of_Ratio"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Alerting Threshold",
    "If the total RX packets dropped per second is at least this percentage of the total RX packets per second, indeni will alert.",
    UIType.DOUBLE,
    new ParameterValue((0.5).asInstanceOf[Object]))

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_rx_drop", "All Devices: RX packets dropped",
    "Indeni tracks the number of packets that had issues and alerts if the ratio is too high.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("network-interface-rx-dropped").last
    val limitValue = TimeSeriesExpression[Double]("network-interface-rx-packets").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("network-interface-rx-dropped", "network-interface-rx-packets")),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-rx-dropped", "network-interface-rx-packets"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          GreaterThanOrEqual(
            TimesExpression(DivExpression(inUseValue, limitValue), ConstantExpression(Some(100.0))),
            getParameterDouble(context, highThresholdParameter))

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"name\")}"),
          scopableStringFormatExpression("The number of RX packets dropped per second is %.0f, out of a total of %.0f packets. This is %.0f%%, where the alerting threshold is %.0f%%.", inUseValue, limitValue, TimesExpression(ConstantExpression[Option[Double]](Some(100.0)), DivExpression(inUseValue, limitValue)), getParameterDouble(context, highThresholdParameter)),
          title = "Affected Ports/Interfaces"
        ).asCondition()
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("Some network interfaces and ports are experiencing a high drop rate. Review the ports below."),
      ConditionalRemediationSteps("Packet drops usually occur when the rate of packets received is higher than the device's ability to handle.",
        ConditionalRemediationSteps.OS_NXOS -> """1. Run the “show interface” command to review the interface counters and the bitrate. Consider to configure the “load-interval 30” interface sub command to improve the accuracy of the interface measurements. Check for traffic bursts and high traffic utilzation.
                                                 |2. Use the “show hardware rate-limit” NX-OS command (if supported) to determine if packets are being dropped because of a rate limit.
                                                 |3. Execute the “show policy-map interface control-plane” NX-OS command to determine if packets are being dropped because of a QoS policy.
                                                 |4. Use the “show hardware internal statistics rates” to determine if packets are being dropped by the hardware.
                                                 |5. Run the “show hardware internal statistics pktflow all” NX-OS command to display per ASIC statistics, including packets into and out of the ASIC. This command helps to identify where packet loss is occurring.""".stripMargin
      )
    )
  }
}
