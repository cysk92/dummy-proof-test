package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.{And, GreaterThan, LesserThan}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}


case class LicenseWillExpireRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Expiration Threshold",
    "How long before expiration should indeni alert.",
    UIType.TIMESPAN,
    TimeSpan.fromDays(56))

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_license_expiration", "All Devices: License expiration nearing",
    "indeni will alert when a license is about to expire.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("license-expiration").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("license-expiration")),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("license-expiration"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            GreaterThan(
              actualValue,
              NowExpression()
            ),
            LesserThan(
              actualValue,
              PlusExpression[Double](NowExpression(), getParameterTimeSpanForTimeSeries(context, highThresholdParameter))))

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"name\")}"),
          scopableStringFormatExpression("Will expire on %s", doubleToDateExpression(actualValue)),
          title = "Affected Licenses"
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more licenses are about to expire. See the list below."),
      ConditionalRemediationSteps("Renew any licenses that need to be renewed.",
        ConditionalRemediationSteps.VENDOR_CP -> "Make sure you have purchased the required licenses and have updated them in your management server: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk33089",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Review this page on licensing: https://www.paloaltonetworks.com/documentation/70/pan-os/pan-os/getting-started/activate-licenses-and-subscriptions",
        ConditionalRemediationSteps.OS_NXOS ->
          """1. Run the “show license usage” NX-OS command to display information about the current license usage and the expire date.
            |2. Run the “show license” NX-OS command to view the installed licenses.
            |3. Run the “show license usage XXX” NX-OS command e.g.” sh license usage ENHANCED_LAYER2_PKG” to display information about the activated features which utilize this license.
            |4. Consider activate the grace-period for the license.
            |5. Order new license from the CISCO.
            |6. For more information please review: <a target="_blank" href="https://www.cisco.com/c/m/en_us/techdoc/dc/reference/cli/nxos/commands/fund/show-license-usage.html">Cisco Guide</a> """.stripMargin
      )
    )
  }
}
