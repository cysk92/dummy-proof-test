package com.indeni.server.rules.library.paloalto

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules._
import com.indeni.server.rules.library.{PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

// TODO Add test
case class WildfireCloudNotConnectedRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("panw_wildfire_cloud_not_connected", "Palo Alto Networks Firewalls: WildFire cloud not connected",
    "If the active member of a cluster loses connectivity to the WildFire cloud, indeni will alert.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("wildfire-connected").last
    val activeMemberValue = TimeSeriesExpression[Double]("cluster-member-active").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      And(
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("cluster-member-active"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          Equals(activeMemberValue, ConstantExpression[Option[Double]](Some(1.0)))
        ).withoutInfo().asCondition(),
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("wildfire-connected")),

          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("wildfire-connected"), denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            Equals(
              inUseValue,
              ConstantExpression[Option[Double]](Some(0.0)))

            // The Alert Item to add for this specific item
          ).withSecondaryInfo(
              scopableStringFormatExpression("${scope(\"name\")}"),
              EMPTY_STRING,
              title = "Clouds Affected"
          ).asCondition()
        ).withoutInfo().asCondition())

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("WildFire connectivity to one or more clouds is down."),
        ConstantExpression("Review https://live.paloaltonetworks.com/t5/Management-Articles/Wildfire-Configuration-Testing-and-Monitoring/ta-p/57722")
    )
  }
}
