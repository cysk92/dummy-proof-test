package com.indeni.server.rules.library.paloalto

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.rules.library.{PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class UserIdAgentDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("panw_user-id_agent_down", "Palo Alto Networks Firewalls: User-ID agent(s) down",
    "If the active member of a cluster has one or more User-ID agents down, indeni will alert.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("userid_agent_state").last
    val activeMemberValue = TimeSeriesExpression[Double]("cluster-member-active").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      And(
          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("cluster-member-active"), denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            Equals(activeMemberValue, ConstantExpression[Option[Double]](Some(1.0)))
          ).withoutInfo().asCondition(),
          StatusTreeExpression(

            // The additional tags we care about (we'll be including this in alert data)
            SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("userid_agent_state")),

              StatusTreeExpression(
                // The time-series we check the test condition against:
                SelectTimeSeriesExpression[Double](context.tsDao, Set("userid_agent_state"), denseOnly = false),

                // The condition which, if true, we have an issue. Checked against the time-series we've collected
                Equals(
                  inUseValue,
                  ConstantExpression[Option[Double]](Some(0.0)))

                // The Alert Item to add for this specific item
              ).withSecondaryInfo(
                  scopableStringFormatExpression("${scope(\"name\")}"),
                  scopableStringFormatExpression("The User-ID agent is not responsive.", inUseValue),
                  title = "User-ID Agents"
              ).asCondition()
          ).withoutInfo().asCondition())

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("One or more User-ID agents are down."),
        ConstantExpression("Check why the User-ID agents listed are not communicating. Refer to https://live.paloaltonetworks.com/t5/Management-Articles/Useful-CLI-Commands-for-Troubleshooting-User-ID-Agent-Software/ta-p/58239 . Useful troubleshooting steps include:\n" +
          "1. Verify if the user agent is connected and operational.\n" +
          "2. Are there IP-to-username mappings?\n" +
          "3. Has the firewall pulled groups from the User-ID agent?\n" +
          "4. Verify the state of the LDAP servers - are they up?")
    )
  }
}
