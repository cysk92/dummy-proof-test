package com.indeni.server.rules.library.paloalto

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules._
import com.indeni.server.rules.library.{PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class NoRxOnHAInterfaceRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("panw_ha_no_rx", "Palo Alto Networks Firewalls: HA interfaces not receiving traffic",
    "A Palo Alto Networks firewall can have up to three HA interfaces. They should all be receiving traffic at all times. If one stops receiving traffic, indeni will alert.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("network-interface-rx-packets").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name", "high-availability-interface"), True),

          StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-rx-packets"), denseOnly = false),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              com.indeni.ruleengine.expressions.conditions.Equals(actualValue, ConstantExpression(Some(0.0)))

              // The Alert Item to add for this specific item
          ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                EMPTY_STRING,
                title = "Affected HA Interfaces"
            ).asCondition()
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("All high-availability interfaces must be receiving traffic at all times. HA1 should be receiving hello/alive pings, HA2 is used for state/session synchronization and HA3 for active/active traffic forwarding. For more information see https://www.paloaltonetworks.com/documentation/70/pan-os/pan-os/high-availability/ha-concepts ."),
        ConstantExpression("Determine why traffic is not being received on the listed ports. Make sure the keep-alive is enabled on HA2. Review https://www.paloaltonetworks.com/documentation/70/pan-os/pan-os/high-availability/configure-active-passive-ha as well.")
    )
  }
}
