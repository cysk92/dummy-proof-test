package com.indeni.server.rules.library

import com.indeni.data.conditions._
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.server.params.{ParameterDefinition, ParameterValue}
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}


case class NicRxOverrunsRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "High_Threshold_of_Ratio"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Alerting Threshold",
    "If the total RX packets flagged as overrun per second is at least this percentage of the total RX packets per second, Indeni will alert.",
    UIType.DOUBLE,
    new ParameterValue((0.5).asInstanceOf[Object]))

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_rx_overrun", "All Devices: RX packets overrun",
    "indeni tracks the number of packets that had issues and alerts if the ratio is too high.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("network-interface-rx-overruns").last
    val limitValue = TimeSeriesExpression[Double]("network-interface-rx-packets").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("network-interface-rx-overruns", "network-interface-rx-packets")),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("network-interface-rx-overruns", "network-interface-rx-packets"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          GreaterThanOrEqual(
            TimesExpression(DivExpression(inUseValue, limitValue), ConstantExpression(Some(100.0))),
            getParameterDouble(context, highThresholdParameter))

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"name\")}"),
          scopableStringFormatExpression("The number of RX packets flagged as overruns per second is %.0f, out of a total of %.0f packets. This is %.0f%%, where the alerting threshold is %.0f%%.", inUseValue, limitValue, TimesExpression(ConstantExpression[Option[Double]](Some(100.0)), DivExpression(inUseValue, limitValue)), getParameterDouble(context, highThresholdParameter)),
          title = "Affected Ports/Interfaces"
        ).asCondition()
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("Some network interfaces and ports are experiencing a high overrun rate. Review the ports below."),
      ConditionalRemediationSteps("Packet overruns usually occur when there are too many packets being inserted into the port's memory buffer, faster than the rate at which the kernel is able to process them.",
        ConditionalRemediationSteps.OS_NXOS -> """In a small number of cases, the overrun counter may be incremented because of a software defect. However, in the majority of cases, it indicates that the receiving capability of the interface was exceeded. Nothing can be done on the device that reports overruns. If possible, the rate that frames are coming should be controlled at the remote end of the connection.
                                                 |1. Run the “show interface” command to review the interface overrun & underun counters and the bitrate. Consider to configure the “load-interval 30” interface sub command to improve the accuracy of the interface measurements.
                                                 |2. If the number of overruns is high, the hardware should be upgraded.
                                                 |
                                                 |In case of high bandwidth utilization:
                                                 |1. Run the “show interface” command to review the interface counters and the bitrate. Consider to configure the “load-interval 30” interface sub command to improve the accuracy of the interface measurements.
                                                 |2. If the interface bitrate is too high and close to the upper bandwidth limit consider to use multiple links with the port-channel technology or upgrade the bandwidth of the link.
                                                 |3. Consider to implement QoS in case of high bandwidth utilization.""".stripMargin
      )
    )
  }
}
