package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.ts.TimeSinceLastValueExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}


case class OspfNeighborIsDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "High_Threshold_of_Downtime"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Downtime",
    "If a peer device is down or not communicating for at least this amount of time, an alert will be issued.",
    UIType.TIMESPAN,
    TimeSpan.fromMinutes(15))

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_ospf_neighbor_down", "All Devices: OSPF neighbor(s) down",
    "Indeni will alert one or more OSPF neighbors isn't communicating well.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val threshold = getParameterTimeSpanForRule(context, highThresholdParameter).noneable

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("ospf-state")),

        StatusTreeExpression(
          SelectTimeSeriesExpression[Double](context.tsDao, Set("ospf-state"), ConstantExpression(TimeSpan.fromDays(1)), denseOnly = false),
          GreaterThan(TimeSinceLastValueExpression(TimeSeriesExpression("ospf-state"), Some(1.0)), threshold)
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"name\")}"),
          EMPTY_STRING,
          title = "Neighbors Affected"
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more OSPF neighbors are down."),
      ConditionalRemediationSteps("Review the cause for the neighbors being down.",
        ConditionalRemediationSteps.VENDOR_CP -> "Review sk84520: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk84520",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Review https://live.paloaltonetworks.com/t5/Configuration-Articles/OSPF-Adjacencies-are-Established-but-the-OSPF-Routes-are-Not/ta-p/58328 . You can also log into the device over SSH and run \"less mp-log routed.log\".",
        ConditionalRemediationSteps.OS_NXOS ->
          """This issue might be caused by:
            |1. L2/L3 connectivity issue
            |2. OSPF not being enabled on the interface
            |3. an interface is defined as passive
            |4. a mismatched subnet mask
            |5. a ismatched hello/dead interval
            |6. a mismatched authentication key
            |7. a mismatched area ID
            |8. a mismatched transit/stub/Not-So-Stubby Area (NSSA) option
            |Check OSPF configuration:
            |Use these commands in order to check the OSPF configuration (subnet, hello/dead interval, area ID, area type, authentication key (if any), and not-passive), and ensure that it matches on both sides.
            |1. "show run ospf"
            |2. "show ip ospf PID interface"
            |3. "show ip ospf PID"
            |
            |Troubleshooting OSPF States:
            |1. OSPF Neighbor Stuck in the Initialization (INIT) State. This issue might be caused by:
            |• One side is blocking the hello packet with ACL
            |• One side is translating, with Network Address Translation (NAT), the OSPF hello
            |• The multicast capability of one side is broken (L2)
            |
            |2. OSPF Neighbor Stuck in a Two-Way State. This issue might be caused by:
            |• OSPF priority set equal to zero
            |NOTE: It is normal in broadcast network types.
            |
            |3. OSPF Neighbor Stuck in Exstart/Exchange. This issue might be caused by:
            |• MTU mismatch
            |• Neighbor Router ID (RID) is the same as its neighbor's
            |• ACL blocking unicast - after two-way OSPF sends unicast packet
            |
            |4. OSPF Neighbor Stuck in a Loading State. This issue might be caused by:
            |• Bad Link State Advertisement (LSA). Check with the show ip ospf bad and the show log command the OSPF-4-BADLSATYPE message.
            |
            |Further details can be found in: <a target="_blank" href="https://www.cisco.com/c/en/us/support/docs/switches/nexus-7000-series-switches/116422-technote-ospf-00.html"> CISCO Nexus OSPF troubleshooting guide|</a> """.stripMargin
      )
    )
  }
}


