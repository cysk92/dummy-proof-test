package com.indeni.server.rules.library

import com.indeni.ruleengine.Status
import com.indeni.ruleengine.expressions.core.{ForExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data.SelectTagsExpression
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.{AvailableDevicesFilterExpression, EnabledAlertFilterExpression, RuleConfigurationExpression}
import com.indeni.server.rules.library.PerAllowedDeviceRule.withOptionalVendor

/**
  * A per-device rule which wraps another per-device rule and adds a filtering level on the devices, which filters
  * out devices that are under maintenance, on a black list, has a licence limitation or were explicitly configured
  * not to be monitored.
  *
  * @param rule The wrapped rule.
  */
case class PerAllowedDeviceRule(rule: PerDeviceRule, context: RuleContext) extends PerDeviceRule {

  /* --- Methods --- */

  /* --- Public Methods --- */

  override def metadata: RuleMetadata = rule.metadata

  override def expressionTree: StatusTreeExpression = {

    val forDeviceAndVendor = withOptionalVendor(rule.expressionTree).statusesEvaluator

    val onlyAvailableDevices =
      AvailableDevicesFilterExpression(context.deviceService, DeviceKey, forDeviceAndVendor.collection)

    val withConfiguration =
      RuleConfigurationExpression(rule.metadata.name,
                                  context.configurationSetDao,
                                  DeviceKey,
                                  ConfigurationSetId,
                                  onlyAvailableDevices)

    val onlyEnabled = EnabledAlertFilterExpression(rule.metadata,
                                                   context.ruleMetadataConfigurationReader,
                                                   ConfigurationSetId,
                                                   withConfiguration)

    StatusTreeExpression(ForExpression[Status](onlyEnabled, forDeviceAndVendor.evaluator))
  }
}

object PerAllowedDeviceRule {

  /* --- Methods --- */

  /* --- Private Methods --- */

  private[library] def withOptionalVendor(expressionTree: StatusTreeExpression): StatusTreeExpression = {

    val forDevice = expressionTree.statusesEvaluator
    val selectDevices = forDevice.collection.asInstanceOf[SelectTagsExpression]
    val selectDevicesWithVendor = selectDevices.copy(optionalKeys = Set(Vendor))

    StatusTreeExpression(ForExpression[Status](selectDevicesWithVendor, forDevice.evaluator))
  }
}

/**
  * A rule that its top/root expression is a for-expression over devices.
  */
trait PerDeviceRule extends Rule {

  /* --- Methods --- */

  /* --- Public Methods --- */

  override def expressionTree: StatusTreeExpression
}
