package com.indeni.server.rules.library.checkpoint

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules._
import com.indeni.server.rules.library.{PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ClusterXLTooManySyncNicsNoVsxRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("clusterxl_too_many_sync_nics_novsx", "Check Point ClusterXL (Non-VSX): More than one sync interface defined",
    "ClusterXL works best with just one sync interface. To achieve redundancy, interface bonding is recommended. indeni will alert when more than one sync interface is used.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("cphaprob-required-secured-interfaces").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), !Equals("vsx", "true")),

      // What constitutes an issue
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("cphaprob-required-secured-interfaces"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          GreaterThan(
            inUseValue,
            ConstantExpression(Some(1.0)))
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("SK92804 and Check Point's best practices recommend you only have one sync interface. To achieve redundancy, a bond interface should be used."),
        ConstantExpression("Read Valeri Loukine's post on the subject: http://checkpoint-master-architect.blogspot.com/2014/05/notes-about-sync-redundancy.html.")
    )
  }
}
