package com.indeni.server.rules.library.checkpoint

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.checkpoint.FirewallKernelLimitNoVsxRule.NAME
import com.indeni.server.rules.library.{PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class FirewallKernelLimitNoVsxRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Kernel_Table_usage"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Kernel Table Usage",
    "What is the threshold for the kernel table usage for which once it is crossed an alert will be issued.",
    UIType.DOUBLE,
    80.0)

  override val metadata: RuleMetadata = RuleMetadata(NAME, "Check Point Firewalls (Non-VSX): Firewall kernel table limit approaching",
    "Check Point firewalls have multiple kernel tables used for on-going traffic processing. If any of them is nearing its limit, an alert will be issued.", AlertSeverity.ERROR, highThresholdParameter)

  /**
    * When alerting about specific kernel tables, we will look for the first match in this key-value sequence and use it to
    * provide further insight regarding the table. NOTE: ORDER IS IMPORTANT.
    */
  val kernelDetailsMap = Seq (
    "(?i)fwx_alloc".r -> "This table stores the NAT connections. Hitting the table limit may result in a drop of NAT connections. Review sk32224.",
    "(?i)fwx_cache".r -> "This table caches the available ports for new NAT connections. Hitting the table limit may result in a slow down of NAT connection initiation. Review sk21834.",
    "(?i)connections".r -> "This table is used to store the current connections going through the firewall. Hitting the table limit may result traffic loss. Review sk39555.",
    "(?i)cphwd_db".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)cphwd_tmpl".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)cphwd_dev_conn_table".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)cphwd_vpndb".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)cphwd_dev_identity_table".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)cphwd_dev_revoked_ips_table".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)cphwd_pslglue_conn_db".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)f2f_addresses".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)tcp_f2f_ports".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)udp_f2f_ports".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)tcp_f2f_conns".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)udp_f2f_conns".r -> "This table is used by SecureXL. Review sk98722.",
    "(?i)dos_suspected".r -> "This table is used by SecureXL. Review sk74520.",
    "(?i)dos_penalty_box".r -> "This table is used by SecureXL. Review sk74520.",
    "(?i)client_auth".r -> "This table is used by the client authentication function. Reaching the limit may result in new connections failing. Review sk41698.",
    "(?i)pdp_sessions".r -> "This table is used by the identity awareness function. Reaching the limit may result in issues with PDP. Review sk101288.",
    "(?i)pdp_super_sessions".r -> "This table is used by the identity awareness function. Reaching the limit may result in issues with PDP. Review sk98342.",
    "(?i)pdp_ip".r -> "This table is used by the identity awareness function. Reaching the limit may result in issues with PDP. Review sk98342.",
    "(?i)crypt_resolver_DB".r -> "This table is used for VPNs with Edge devices. Review sk101680.",
    "(?i)cluster_active_robo".r -> "This table is used for VPNs with Edge devices. Review sk101680.",
    "(?i)appi_connections".r -> "This table is used for URL Filtering. Review sk95588.",
    "(?i)cluster_connections_nat".r -> "This table is used for VPN handling in clusters. Review sk104760.",
    "(?i)cryptlog_table".r -> "This table is used for debugging VPNs. If this table fills up it may mean there's a critical underlying issue with vpnd. Review sk104760.",
    "(?i)DAG_ID_to_IP".r -> "This table is used for mapping DAIP gateways. Review sk104760.",
    "(?i)DAG_IP_to_ID".r -> "This table is used for mapping DAIP gateways. Review sk104760.",
    "(?i)decryption_pending".r -> "This table is used for RDP negotiation. Review sk104760.",
    "(?i)encryption_requests".r -> "This table is used for storing information of new encryption requests. Review sk104760.",
    "(?i)IKE_SA_table".r -> "This table is used for storing all the ISAKMP SAs. Review sk104760.",
    "(?i)ike2esp".r -> "This table is used for storing IKE-to-ESP mappings. Review sk104760.",
    "(?i)ike2peer".r -> "This table is used for storing IKE-to-Peer mappings. Review sk104760.",
    "(?i)inbound_SPI".r -> "This table is used for storing inbound IPSec negotations. Review sk104760.",
    "(?i)initial_contact_pending".r -> "This table is used for storing IKE cookies. Review sk104760.",
    "(?i)ipalloc_tab".r -> "This table is used for managing IP address lease extension requests. Review sk104760.",
    "(?i)IPSEC_mtu_icmp".r -> "This table is used for maintaing MTU information for specific source IPs. Review sk104760.",
    "(?i)IPSEC_mtu_icmp_wait".r -> "This table is used for maintaing MTU information for specific source IPs. Review sk104760.",
    "(?i)L2TP_lookup".r -> "This table is used for storing L2TP sessions. Review sk104760.",
    "(?i)L2TP_MSPI_cluster_update".r -> "This table is used for mapping MSPI's. Review sk104760.",
    "(?i)L2TP_sessions".r -> "This table is used for storing L2TP sessions. Review sk104760.",
    "(?i)L2TP_tunnels".r -> "This table is used for storing L2TP tunnels. Review sk104760.",
    "(?i)MSPI_by_methods".r -> "This table provides a way to access an actual SA, through MSA, which is pointed to by the MSPI. Review sk104760.",
    "(?i)MSPI_cluster_map".r -> "This table maps between MSPI on cluster peer members, to the MSPI on this cluster member. Review sk104760.",
    "(?i)MSPI_cluster_update".r -> "This table is used for synchronization of the MSPI numbers between the cluster members. Review sk104760.",
    "(?i)MSPI_cluster_reverse_map".r -> "This table is used to map between MSPI on this cluster member to MSPI on cluster peer member. Review sk104760.",
    "(?i)MSPI_req_connections".r -> "This table is used with MSPI_requests table. A packet that arrives for a connection that has issued a request for an MSPI, either because it does not exist, or the underlying SA is expired, is dropped. Review sk104760.",
    "(?i)MSPI_requests".r -> "This table holds a linked list of packet IDs, which are on hold for an MSPI. Review sk104760.",
    "(?i)outbound_SPI".r -> "This table contains all Outbound IPSec SAs. Review sk104760.",
    "(?i)peer2ike".r -> "This table holds Peer-to-IKE mappings for cookies. Review sk104760.",
    "(?i)peers_count".r -> "This table is used store all the Gateway-to-Gateway peers, with which the local gateway maintains IPsec tunnels. Review sk104760.",
    "(?i)persistent_tunnels".r -> "This table holds MSPIs of persistent IPsec tunnels. Review sk104760.",
    "(?i)rdp_dont_trap".r -> "This table relates to outbound RDP traffic. Review sk104760.",
    "(?i)rdp_table".r -> "This table holds RDP connections in the following specific case: when two VPN peers perform encryption with one another, and there is a Security Gateway in the middle that needs to forward these RDP connections. Review sk104760.",
    "(?i)resolved_link".r -> "This table holds the resolved link to the peer gateway. Review sk104760.",
    "(?i)Sep_my_IKE_packet".r -> "This table is used for IKE forwarding in Load Sharing mode clusters. Review sk104760.",
    "(?i)SPI_requests".r -> "This table is used by the VPN kernel to mark requests for SPI on behalf of a connection. Review sk104760.",
    "(?i)udp_enc_cln_table".r -> "This table maintains a list of the destination ports and the IP address used for UDP encapsulation. Review sk104760.",
    "(?i)udp_response_nat".r -> "This table enables translation of outbound IKE and RDP packets, so that their source IP address appears to be the same as the destination IP address of the original packets (previous inbound from the same peer). Review sk104760.",
    "(?i)VIN_SA_to_delete".r -> "This table holds the IPsec NIC SA data until the packet's reference countdown reaches 0. Review sk104760.",
    "(?i)vpn_active".r -> "This table maintains a flag indicating if the VPN module is active. Review sk104760.",
    "(?i)vpn_routing".r -> "This table holds all the possible ranges that were defined for all Security Gateways. Review sk104760.",
    "(?i)XPO_names".r -> "This table is used when extranet partners are defined. It maps, for each partner, the XPO_ID, a UUID that is automatically assigned to each partner in the extranet, to a string ID that is used internally to write the name of the partner in the logs. Review sk104760.",
    "(?i)vpn_queues".r -> "This table is related to VPN. Review sk112141.",
    "(?i)ikev2_sas".r -> "This table is related to VPN. Review sk112141.",
    "(?i)sam_requests".r -> "This table is related to SAM rules. Review sk101368.",
    "(?i)outbound_SPI".r -> "This table contains all Outbound IPSec SAs. Review sk104760.",
    "(?i)outbound_SPI".r -> "This table contains all Outbound IPSec SAs. Review sk104760.",
    "(?i)outbound_SPI".r -> "This table contains all Outbound IPSec SAs. Review sk104760.",
    "(?i)tnlmon_life_sign".r -> "This table tracks the monitoring of permanent tunnels. If it fills up, the firewall may have issues tracking permanent tunnels.",
    "(?i)cptls_server_cn_cache".r -> "This table servers as a cache of IPs and certificate's canonical names. Review sk92743.",


    // Catch-all
    ".*".r -> "Please consult with your technical support provider about this kernel table."
  )

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("kernel-table-actual").last
    val limitValue = TimeSeriesExpression[Double]("kernel-table-limit").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), !Equals("vsx", "true")),

      // What constitutes an issue
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("kernel-table-actual", "kernel-table-limit")),

            StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("kernel-table-actual", "kernel-table-limit"), denseOnly = false),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              GreaterThanOrEqual(
                inUseValue,
                TimesExpression[Double](limitValue, DivExpression[Double](getParameterDouble(context, highThresholdParameter), ConstantExpression[Option[Double]](Some(100.0)))))

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"name\")}"),
                new ScopableExpression[String] {

                  override protected def evalWithScope(time: Long, scope: Scope): String = {
                    val name = scope.getVisible("name").get.toString
                    val tableDetails = kernelDetailsMap.collectFirst {
                      case item if (!item._1.findFirstMatchIn(name).isEmpty) => item._2
                    }.get
                    inUseValue.eval(time).get.toInt + " entries in use vs a limit of " + limitValue.eval(time).get.toInt + ". " + tableDetails
                  }

                  override def args: Set[Expression[_]] = Set()
                },
                title = "Affected Kernel Tables"
            ).asCondition()
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("Some firewall kernel tables are nearing their limit. Review the list below.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/in/motisagey\">Moti Sagey</a>."),
        ConstantExpression("Review the specific tables, determine what they are used for " +
          "and why they are approaching capacity. It is possible that " +
          "certain configuration changes should be made to increase the capacity " +
          "of a table in order to allow it to handle larger loads.")
    )
  }
}

object FirewallKernelLimitNoVsxRule {

  /* --- Constants --- */

  private[checkpoint] val NAME = "chkp_firewall_kernel_table_limit_no_vsx"
}
