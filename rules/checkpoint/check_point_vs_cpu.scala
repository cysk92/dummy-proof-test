package com.indeni.server.rules.library.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NumericThresholdOnDoubleMetricWithItemsTemplateRule

case class check_point_vs_cpu(context: RuleContext) extends NumericThresholdOnDoubleMetricWithItemsTemplateRule(context,
    ruleName = "check_point_vs_cpu",
    ruleFriendlyName = "Check Point (VSX): Some VSes have high CPU usage",
    ruleDescription = "indeni will alert when a virtual system's CPU utilization is too high.",
    metricName = "vs-cpu-usage",
    threshold = 70.0,
    applicableMetricTag = "vs.name",
    alertItemsHeader = "Affected Virtual Systems",
    alertItemDescriptionFormat = "The current CPU usage is %.0f%%",
    alertDescription = "Some VSes have high CPU utilization. This could mean slowdown of traffic or packet loss.",
    baseRemediationText = "Determine the cause for the high CPU usage of the listed cores. This may indicate a need for more cores needs to be added.\nReview the following article for further information on high CPU utilization on Check Point firewalls. https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk98348"
)()
