package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.LesserThan
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.MinusExpression
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class PanwAppPackageNotUpdatedRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("panw_app_lag_check", "Palo Alto Networks Firewalls: Application package not up to date",
    "indeni will alert if the application package has not been updated according to schedule.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val installedPackageDate = TimeSeriesExpression[Double]("panw-installed-app-release-date").last
    val currentTime = TimeSeriesExpression[Double]("current-datetime").last
    val acceptableLag = TimeSeriesExpression[Double]("app-update-acceptable-lag").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("panw-installed-app-release-date", "current-datetime", "app-update-acceptable-lag"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          LesserThan(
            acceptableLag,
            MinusExpression[Double](currentTime, installedPackageDate))

          // The Alert Item to add for this specific item
      ).withRootInfo(
            getHeadline(),
            scopableStringFormatExpression("The current app update package's release date is %s. This indicates the update schedule is not operating correctly.", doubleToDateExpression(installedPackageDate)),
            ConstantExpression("Review possible causes for the update package not being downloaded and installed.")
        ).asCondition()
    ).withoutInfo()
  }
}
