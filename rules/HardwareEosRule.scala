package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.LesserThan
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class HardwareEosRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Expiration Threshold",
    "How long before end of support should indeni alert.",
    UIType.TIMESPAN,
    TimeSpan.fromDays(365))

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_hardware_eos", "All Devices: Hardware end of support nearing",
    "indeni will alert a significant time before the device hardware reaches end of support.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("hardware-eos-date").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("hardware-eos-date"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          LesserThan(
            actualValue,
            PlusExpression[Double](NowExpression(), getParameterTimeSpanForTimeSeries(context, highThresholdParameter)))

          // The Alert Item to add for this specific item
          ).withRootInfo(
            getHeadline(),
            scopableStringFormatExpression("The end of support for this device's hardware is on %s.", doubleToDateExpression(actualValue)),
            ConditionalRemediationSteps("Upgrade the hardware to a more recently released device.",
              ConditionalRemediationSteps.VENDOR_CP -> "The full information on Check Point's hardware end of support is available at: https://www.checkpoint.com/support-services/support-life-cycle-policy/",
              ConditionalRemediationSteps.VENDOR_PANOS -> "Review https://www.paloaltonetworks.com/services/support/end-of-life-announcements/hardware-end-of-life-dates"
            )
        ).asCondition()
    ).withoutInfo()
  }
}
