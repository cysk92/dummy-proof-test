package com.indeni.server.rules.library

import com.indeni.data.conditions.{TagsStoreCondition, True}
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.Condition
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.util.matching.Regex

class MultiSnapshotValueCheckTemplateRule(context: RuleContext, ruleName: String, ruleFriendlyName: String, ruleDescription: String,
                                               severity: AlertSeverity = AlertSeverity.ERROR,
                                               metricName: String, applicableMetricTag: String = null, alertDescription: String,
                                               alertItemsHeader: String = null, baseRemediationText: String,
                                               complexCondition: Condition,
                                               metaCondition: TagsStoreCondition = True, itemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> ""))(vendorToRemediationText: (String, String)*) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata(ruleName, ruleFriendlyName, ruleDescription, severity)

  override def expressionTree: StatusTreeExpression = {
    val value = SnapshotExpression(metricName).asMulti().mostRecent()

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      // What constitutes an issue
      if (null != applicableMetricTag) StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.snapshotsDao, Set(applicableMetricTag), withTagsCondition(metricName)),

          StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).multi(),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
            complexCondition
          ).withSecondaryInfo(
              scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}"),
              new ScopableExpression[String] {
                override protected def evalWithScope(time: Long, scope: Scope): String = {
                  val metricTagValue = scope.getVisible(applicableMetricTag).get.toString
                  itemSpecificDescription.collectFirst {
                    case item if (!item._1.findFirstMatchIn(metricTagValue).isEmpty) => item._2
                  }.get + value.eval(time).map(_.toList.sortBy(_._1).map(x => s"${x._1}: ${x._2}").mkString(", ")).mkString("\n")
                }

                override def args: Set[Expression[_]] = Set(value)
              },
              title = alertItemsHeader
          ).asCondition()
       ).withoutInfo().asCondition()
      else StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).multi(),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        complexCondition
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression(alertDescription),
        ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText:_*)
    )
  }
}
