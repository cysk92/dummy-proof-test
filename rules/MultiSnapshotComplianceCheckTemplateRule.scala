package com.indeni.server.rules.library

import com.indeni.data.conditions.{TagsStoreCondition, True}
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.core.{ConstantExpression, EMPTY_STRING, MultiIssueInformer, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.ruleengine.expressions.utility.{SeqDiffMissingsExpression, SeqDiffRedundantsExpression, SeqDiffWithoutOrderExpression}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.sensor.models.automationpolicy.AutomationPolicyItemAlertSetting
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

class MultiSnapshotComplianceCheckTemplateRule(context: RuleContext, ruleName: String, ruleFriendlyName: String, ruleDescription: String,
                                               severity: AlertSeverity = AlertSeverity.ERROR,
                                               metricName: String, itemKey: String, alertDescription: String,
                                               baseRemediationText: String,
                                               metaCondition: TagsStoreCondition = True,
                                               requiredItemsParameterName: String, requiredItemsParameterDescription: String)(vendorToRemediationText: (String, String)*) extends PerDeviceRule with RuleHelper {

  private val requiredItems = new ParameterDefinition(
    "required_items",
    "",
    requiredItemsParameterName,
    requiredItemsParameterDescription,
    UIType.MULTILINE_TEXT,
    "")

  override val metadata: RuleMetadata = RuleMetadata(ruleName, ruleFriendlyName, ruleDescription, severity, defaultAction = Some(AutomationPolicyItemAlertSetting.NEVER), requiredItems)

  override def expressionTree: StatusTreeExpression = {
    val itemTag = "item_tag"
    val itemsExpected = DynamicParameterExpression.withConstantDefault(context.parametersDao, metadata.name, requiredItems.getName, ConfigurationSetId, Seq[String]()).withLazy
    val diff = SeqDiffWithoutOrderExpression[String](
      MultiSnapshotExtractScalarExpression(SnapshotExpression(metricName).asMulti().mostRecent(), itemKey),
      itemsExpected
    ).withLazy

    val headline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = scope.getVisible(itemTag).get.toString

      override def args: Set[Expression[_]] = Set()
    }

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The snapshot we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).multi(),

        // The condition which, if true, we have an issue. Checked against the snapshots we've collected
        diff.nonEmpty,
        multiInformers =
          Set(
            MultiIssueInformer(
              headline,
              EMPTY_STRING,
              "Missing Items"
            ).iterateOver(SeqDiffMissingsExpression(diff),
              itemTag,
              com.indeni.ruleengine.expressions.conditions.True
            ),
            MultiIssueInformer(
              headline,
              EMPTY_STRING,
              "Redundant Items"
            ).iterateOver(
              SeqDiffRedundantsExpression(diff),
              itemTag,
              com.indeni.ruleengine.expressions.conditions.True
            )
          )
        // Details of the alert itself
      ).withRootInfo(
          getHeadline(),
        ConstantExpression(alertDescription),
        ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText:_*)
      ).asCondition()
    ).withoutInfo()
  }
}
