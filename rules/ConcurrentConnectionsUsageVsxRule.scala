package com.indeni.server.rules.library

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.ruleengine.expressions.utility.NoneableExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.ConcurrentConnectionsUsageVsxRule.NAME
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class ConcurrentConnectionsUsageVsxRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Connection_usage"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Concurrent Connection Usage",
    "What is the threshold for the concurrent connection usage for which once it is crossed an alert will be issued.",
    UIType.DOUBLE,
    80.0)

  override val metadata: RuleMetadata = RuleMetadata(NAME, "All Devices: Per-virtual-system concurrent connection limit nearing",
    "indeni will alert the number of connections in a VS is too high.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("concurrent-connections").last
    val threshold: NoneableExpression[Double] = getParameterDouble(context, highThresholdParameter)
    val limit = TimeSeriesExpression[Double]("concurrent-connections-limit").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("vs.id","vs.name"), True),

            StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("concurrent-connections", "concurrent-connections-limit"), denseOnly = false),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              GreaterThanOrEqual(
                actualValue,
                TimesExpression(limit, DivExpression(threshold, ConstantExpression(Some(100.0)))))

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"vs.name\")} (${scope(\"vs.id\")})"),
                scopableStringFormatExpression("Usage of %.0f (vs limit of %.0f) is above the threshold of %.0f%%.", actualValue, limit, threshold),
                title = "Affected VS's"
            ).asCondition()
        ).withoutInfo().asCondition()
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("Some VS's have a high number of concurrent connections.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/in/motisagey\">Moti Sagey</a>."),
        ConditionalRemediationSteps("Review why this may be happening and consider moving some of the traffic between VS's or devices.",
          ConditionalRemediationSteps.VENDOR_CP -> "Consider enabling aggressive aging if it is not yet enabled: https://sc1.checkpoint.com/documents/R76/CP_R76_IPS_AdminGuide/12857.htm#o12861",
          ConditionalRemediationSteps.VENDOR_PANOS -> "Compare the products and the maximum sessions allowed: https://www.paloaltonetworks.com/products/product-selection"
      )
    )
  }
}

object ConcurrentConnectionsUsageVsxRule {

  /* --- Constants --- */

  private[library] val NAME = "concurrent_connection_limit_vsx"
}

