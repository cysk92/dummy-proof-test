package com.indeni.server.rules.library.templatebased.crossvendor.compliance

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SingleSnapshotComplianceCheckTemplateRule}

case class crossvendor_compliance_check_os_version(context: RuleContext) extends SingleSnapshotComplianceCheckTemplateRule(context,
  ruleName = "crossvendor_compliance_check_os_version",
  ruleFriendlyName = "Compliance Check: OS/Software version does not match requirement",
  ruleDescription = "Indeni can verify that the OS/software version installed is a specific one.",
  metricName = "os-version",
  baseRemediationText = "Install the OS/software version required.",
  parameterName = "OS/Software Version",
  parameterDescription = "The OS/software version to compare against.",
  expectedValue = "")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Check that the vPC peers have the same NX-OS version except during the non-disruptive upgrade, that is, In-Service Software Upgrade (ISSU).
      |2. Execute the "show version" NX-OS command and check the installed NX-OS version across the vPC peer switches.
      |3. Schedule a Maintenance Window for NX-OS upgrade in order the vPC peer switches have exact the same NX-OS version.
      |4. You can follow the next NX-OS upgrade guides for Nexus 9k, 7k, 5k and 3k series:
      |
      |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/6-x/upgrade/guide/b_Cisco_Nexus_9000_Series_NX-OS_Software_Upgrade_and_Downgrade_Guide_Release_6x/b_Cisco_Nexus_9000_Series_NX-OS_Software_Upgrade_and_Downgrade_Guide_Release_6x_chapter_01.html
      |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/6_x/nx-os/upgrade/guide/b_Cisco_Nexus_7000_Series_NX-OS_Software_Upgrade_and_Downgrade_Guide_Release_6-x.html
      |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/upgrade/503_N1_1/n5k_upgrade_downgrade_503.html
      |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus3000/sw/upgrade/6_x/Cisco_n3k_Upgrade_Downgrade_6x.html
    """.stripMargin
)
