package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}

/**
  *
  */
case class static_routing_table_comparison_vsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "static_routing_table_comparison_vsx",
  ruleFriendlyName = "Clustered Devices (VS): Static routing table does not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if their static routing tables are different.",
  metricName = "static-routing-table",
  applicableMetricTag = "vs.name",
  isArray = true,
  alertDescription = "Devices that are part of a cluster must have the same static routing tables. Review the differences below.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/itzik-assaraf/2/870/1b5\">Itzik Assaraf</a> (Leumi Card).",
  baseRemediationText = "Ensure the static routing table matches across devices in a cluster.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Use the \"show configuration\" command in clish to compare the calls to \"set static-route\".",
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Execute the "show ip route static" command to display the current contents of the  static routes installed to the routing table.
      |2. Compare the static route config between the peer switches with the show run | i "ip route" command
      |NOTE: The static routes configured between the peer switches may be different in case of orphan devices without need of redundancy between the vPC peer switches
      |3. For more information please review the next Cisco configuration guide:
      |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/5_x/nx-os/unicast/configuration/guide/l3_cli_nxos/l3_route.html
    """.stripMargin
)
