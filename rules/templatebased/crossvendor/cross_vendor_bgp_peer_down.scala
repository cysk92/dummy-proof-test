package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class cross_vendor_bgp_peer_down(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_bgp_peer_down",
  ruleFriendlyName = "All Devices: BGP peer(s) down",
  ruleDescription = "Indeni will alert one or more BGP peers isn't communicating well.",
  metricName = "bgp-state",
  applicableMetricTag = "name",
  alertItemsHeader = "Peers Affected",
  alertDescription = "One or more BGP peers are down.",
  baseRemediationText = "Review the cause for the peers being down.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Consider reading Tobias Lachmann's blog on BGP: https://blog.lachmann.org/?p=1771",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Consider starting at https://live.paloaltonetworks.com/t5/Configuration-Articles/BGP-Routes-are-not-Injected-into-the-Routing-Table/ta-p/54938 . You can also log into the device over SSH and run \"less mp-log routed.log\".",
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Get information for all BGP neighbors by running the "show bgp vrf all sessions" NX-OS command
      |2. Get a summary list of BGP neighbors and statistics by executing the "show ip bgp vrf all summary" NX-OS command
      |3. Get detailed information from a BGP neighbor by running the "show ip bgp neighbors X.X.X.X" NX-OS command
      |4. Check global BGP process information with the "show bgp process" NX-OS command
      |5. Review the logs for relevant findings
      |6. For more information please review  following CISCO BGP troubleshooting flow chart:
      |https://www.cisco.com/c/en/us/support/docs/ip/border-gateway-protocol-bgp/22166-bgp-trouble-main.html#anc6""".stripMargin
)
