package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_vuln_sweet32(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_vuln_sweet32",
  ruleFriendlyName = "All Devices: Device is vulnerable to SWEET32",
  ruleDescription = "indeni will check if a device is currently vulnerable to SWEET32.",
  metricName = "vuln-sweet32",
  alertDescription = "This device, in its current configuration, is vulnerable to SWEET32 ( https://sweet32.info/ ). This means that an eavesdropper may be able to decrypt certain important traffic packets.",
  baseRemediationText = "Follow the vendor-recommended remediation steps.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("vuln-sweet32").asSingle().mostRecent().noneable)
)(ConditionalRemediationSteps.VENDOR_CP -> "https://supportcontent.checkpoint.com/solutions?id=sk113114",
  ConditionalRemediationSteps.VENDOR_F5 -> "https://support.f5.com/csp/#/article/K13167034",
  ConditionalRemediationSteps.VENDOR_PANOS -> "https://live.paloaltonetworks.com/t5/Threat-Articles/Information-on-Sweet32-for-Palo-Alto-Networks-Customers/ta-p/128526")
