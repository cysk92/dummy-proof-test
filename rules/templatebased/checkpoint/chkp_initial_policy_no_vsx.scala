package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class chkp_initial_policy_no_vsx(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "chkp_initial_policy_no_vsx",
  ruleFriendlyName = "Check Point Firewalls (Non-VSX): Firewall policy in InitialPolicy",
  ruleDescription = "indeni will alert when a Check Point firewall is running with the InitialPolicy policy.",
  metricName = "policy-installed-fingerprint",
  alertDescription = "It appears the firewall is in InitialPolicy. This may result in traffic disruption.",
  baseRemediationText = "Ensure a valid policy is installed.",
  metaCondition = !DataEquals("vsx", "true"),
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("InitialPolicy"), SnapshotExpression("policy-installed-fingerprint").asSingle().mostRecent().noneable)
)()
