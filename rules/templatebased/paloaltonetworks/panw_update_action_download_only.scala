package com.indeni.server.rules.library.templatebased.paloaltonetworks

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class panw_update_action_download_only(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "panw_update_action_download_only",
  ruleFriendlyName = "Palo Alto Networks Firewalls: Update schedule set to download only",
  ruleDescription = "indeni will alert if the update schedule for application packages is set to download only.",
  metricName = "panw-app-update-action",
  alertDescription = "The update schedule is set to download only (without installation). This is against Palo Alto Networks best practices.",
  baseRemediationText = "In the update schedule, select Download and Install.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("download-only"), SnapshotExpression("panw-app-update-action").asSingle().mostRecent().noneable)
)()
