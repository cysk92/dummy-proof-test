package com.indeni.server.rules.library.templatebased.f5

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class f5_ssl_weak_protocol(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "f5_ssl_weak_protocol",
  ruleFriendlyName = "F5 Devices: Weak security protocol used with SSL profiles",
  ruleDescription = "Certain security protocols are now considered weak. indeni will alert if any SSL profiles are set to use them.",
  metricName = "ssl-weak-protocol",
  applicableMetricTag = "name",
  descriptionMetricTag = "protocol",
  alertIfDown = false,
  alertItemsHeader = "Affected Profiles",
  alertDescription = "Certain SSL profiles have a weak security protocol and are potentially opening traffic to known vulnerabilities.",
  baseRemediationText = "Follow the knowledge articles listed in the affected profiles above.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  itemSpecificDescription = Seq(
    ".*SSLv3.*".r -> "This profile is using a vulnerable communication protocol. See https://support.f5.com/csp/#/article/K15702",
    ".*SSLv2.*".r -> "This profile is using a vulnerable communication protocol. See https://support.f5.com/csp/#/article/K23196136",
    ".*".r -> ""))()
