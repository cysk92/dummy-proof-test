package com.indeni.server.rules.library.templatebased.f5

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class f5_wildcard_forwarding_servers_used(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "f5_wildcard_forwarding_servers_used",
  ruleFriendlyName = "F5 Devices: A virtual forwarding server is listening for traffic with a destination of any on all VLANs",
  ruleDescription = "Using a virtual forwarding server in a large network in combination with All VLANs would short circuit networks behind the load balancer and this is not ideal in terms of security. indeni will alert if this configuration is used.",
  metricName = "f5-wildcard-forwarding-servers",
  applicableMetricTag = "name",
  alertItemsHeader = "Virtual Servers Affected",
  alertDescription = "Forwarding servers is a way of using the F5 as a router to move packets from one network to another. One does this by configuring a special virtual server type listening to the destination.\n\nExample:\nA virtual server listening to 10.0.0.0/8 on VLAN10 would accept, and forward packets according to its routing table, but only on VLAN10.\n\nUsing a virtual forwarding server in a large network in combination with All VLANs would short circuit networks behind the load balancer and this is not ideal in terms of security.",
  baseRemediationText = "Verify that the configuration is intentional. If not, create forwarding servers for each VLAN listening on the egress VLAN, and one forwarding server listening on all VLANs except the egress VLANs. This way you allow traffic to pass through the load balancer without short circuiting the VLANs behind it.\n\nNote: A change window is highly recommended as there may be impact to the environment. More information about virtual forwarding servers can be found here: https://support.f5.com/csp/article/K7595\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("true"), SnapshotExpression("f5-wildcard-forwarding-servers").asSingle().mostRecent().noneable))()
