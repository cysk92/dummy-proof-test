package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.scope.ScopeValueExpression
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class PermanentOrMonitoredVpnTunnelIsDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_permanent_tunnel_down", "Firewall Devices: Permanent/Monitored VPN tunnel(s) down",
    "Some VPN tunnels are set to be permanent, or monitored, to ensure they are always up. indeni will alert if such VPN tunnels are down.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("vpn-tunnel-state").last
    val alwaysOn = ScopeValueExpression("always-on").visible().asString().noneable

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("peerip", "name", "always-on"), True),

          // The case here is different compared to VpnTunnelIsDownRule, because we only take
          // the tunnels which are always on
          StatusTreeExpression(
            SelectTimeSeriesExpression[Double](context.tsDao, Set("vpn-tunnel-state"), denseOnly = false),
            And(
              Equals(actualValue, ConstantExpression(Some(0.0))),
              Equals(alwaysOn, ConstantExpression(Some("true")))
            )

            // The Alert Item to add for this specific item
            ).withSecondaryInfo(
              scopableStringFormatExpression("${scope(\"name\")} (${scope(\"peerip\")})"),
              scopableStringFormatExpression("This tunnel is down even though it is set to be always up."),
              title = "VPN Tunnels Affected"
          ).asCondition()
        ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
        ConstantExpression("One or more VPN tunnels which should remain up is now down."),
        ConstantExpression("Review the cause for the tunnels being down.")
    )
  }
}


