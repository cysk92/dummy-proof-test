package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.{AbsExpression, MinusExpression}
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}


case class ClockOffRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_clock_off", "All Devices: Clock set incorrectly",
    "Indeni will alert when a device's clock is more than 24 hours off of Indeni's clock.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("current-datetime").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set("current-datetime"), denseOnly = false),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        GreaterThan(
          AbsExpression(MinusExpression(actualValue, NowExpression())),
          ConstantExpression(Some(3600.0 * 24)))

        // The Alert Item to add for this specific item
      ).withRootInfo(
        getHeadline(),
        scopableStringFormatExpression("The current date/time on this device is: %s which seems to be incorrect.", doubleToDateExpression(actualValue)),
        ConditionalRemediationSteps("Consider setting the date on the device and activating NTP.",
          ConditionalRemediationSteps.OS_NXOS -> """1. Execute the "show clock" command to check the system time.
                                                   |2. Consider setting the date on the device by activating NTP.
                                                   |3. For more information please review: <a target="_blank" href="https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/5_x/nx-os/system_management/configuration/guide/sm_nx_os_cg/sm_3ntp.pdf">Cisco NX-OS NTP configuration guide</a>""".stripMargin
        )
      ).asCondition()
    ).withoutInfo()
  }
}
