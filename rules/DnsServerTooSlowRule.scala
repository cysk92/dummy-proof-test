package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.AverageExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class DnsServerTooSlowRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Response Time Threshold (ms)",
    "If a DNS server's response time is higher than this, an alert will be issued..",
    UIType.DOUBLE,
    150)

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_dns_server_response_time", "All Devices: DNS server response time slow",
    "indeni will alert when a DNS server takes too long to respond.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = AverageExpression(TimeSeriesExpression[Double]("dns-response-time"))

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("dns-server"), True),

        StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("dns-response-time"), historyLength = ConstantExpression(TimeSpan.fromMinutes(60)), denseOnly = false),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              GreaterThanOrEqual(
                actualValue,
                getParameterDouble(context, highThresholdParameter))

              // The Alert Item to add for this specific item
            ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"dns-server\")}"),
                scopableStringFormatExpression("This server's response time is: %.0f ms", actualValue),
                title = "Affected Servers"
            ).asCondition()
        ).withoutInfo().asCondition()


      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("One or more DNS servers have a high response time. This may slow down critical system processes."),
        ConstantExpression("Identify any network and server issues which may be causing this.")
    )
  }
}
