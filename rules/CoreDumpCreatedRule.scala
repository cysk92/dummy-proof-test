//package com.indeni.server.rules.library
//
//import com.indeni.data.conditions.True
//import com.indeni.ruleengine._
//import com.indeni.ruleengine.expressions.conditions.{And, GreaterThan, LesserThan}
//import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
//import com.indeni.ruleengine.expressions.data._
//import com.indeni.ruleengine.expressions.math.PlusExpression
//import com.indeni.ruleengine.expressions.utility.NowExpression
//import com.indeni.server.params.ParameterDefinition
//import com.indeni.server.params.ParameterDefinition.UIType
//import com.indeni.server.rules._
//import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
//import com.indeni.time.TimeSpan
//
//case class CoreDumpCreatedRule(context: Context) extends PerDeviceRule with RuleHelper {
//
//  private[library] val highThresholdParameterName = "Alerting_Threshold"
//  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
//    "",
//    "Alerting Threshold",
//    "How long ago should the core dump file be created for indeni to alert.",
//    UIType.TIMESPAN,
//    TimeSpan.fromDays(56))
//
//  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_core_dump_created", "All Devices: Core dump files found",
//    "indeni will alert when a core dump file is created.", AlertSeverity.ERROR, highThresholdParameter)
//
//  override def expressionTree: StatusTreeExpression = {
//    val coredumps = SnapshotExpression("core-dumps").asMulti().mostRecent()
//
//    StatusTreeExpression(
//      // Which objects to pull (normally, devices)
//      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
//
//      // What constitutes an issue
//      StatusTreeExpression(
//        // The time-series we check the test condition against:
//        SelectSnapshotsExpression(context.snapshotsDao, Set("core-dumps")).single(),
//
//        // The condition which, if true, we have an issue. Checked against the time-series we've collected
//        And(
//          GreaterThan(
//            actualValue,
//            NowExpression()
//          ),
//          LesserThan(
//            actualValue,
//            PlusExpression[Double](NowExpression(), getParameterTimeSpanForTimeSeries(context, highThresholdParameter)))),
//
//        // The Alert Item to add for this specific item
//        StatusCoreInformerExpression(
//          scopableStringFormatExpression("${scope(\"name\")}"),
//          scopableStringFormatExpression("Will expire on %s", doubleToDateExpression(actualValue)),
//          EMPTY_STRING),
//      ).asCondition(),
//      StatusCoreInformerExpression(
//        getHeadline(),
//        ConstantExpression("One or more licenses are about to expire. See the list below."),
//        ConditionalRemediationSteps("Renew any licenses that need to be renewed.",
//          ConditionalRemediationSteps.VENDOR_CP -> "Make sure you have purchased the required licenses and have updated them in your management server: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk33089",
//          ConditionalRemediationSteps.VENDOR_PANOS -> "Review this page on licensing: https://www.paloaltonetworks.com/documentation/70/pan-os/pan-os/getting-started/activate-licenses-and-subscriptions"
//        ))
//    )
//  }
//}
