package com.indeni.server.rules.library

import com.indeni.data.conditions.{TagsStoreCondition, True}
import com.indeni.ruleengine.expressions.casting.ToStringExpression
import com.indeni.ruleengine.expressions.conditions.Equals
import com.indeni.ruleengine.expressions.core.StatusTreeExpression
import com.indeni.ruleengine.expressions.data._
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.sensor.models.automationpolicy.AutomationPolicyItemAlertSetting
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

class SingleSnapshotComplianceCheckTemplateRule[T: scala.reflect.runtime.universe.TypeTag](context: RuleContext, ruleName: String, ruleFriendlyName: String, ruleDescription: String,
                                                severity: AlertSeverity = AlertSeverity.ERROR,
                                                metricName: String,
                                                baseRemediationText: String,
                                                metaCondition: TagsStoreCondition = True,
                                                expectedValue: T,
                                                parameterName: String, parameterDescription: String)(vendorToRemediationText: (String, String)*) extends PerDeviceRule with RuleHelper {

  private val requiredValue = new ParameterDefinition(
    "required_value",
    "",
    parameterName,
    parameterDescription,
    UIType.fromObjectClass(expectedValue.getClass),
    expectedValue)

  override val metadata: RuleMetadata = RuleMetadata(ruleName, ruleFriendlyName, ruleDescription, severity, defaultAction = Some(AutomationPolicyItemAlertSetting.NEVER), requiredValue)

  override def expressionTree: StatusTreeExpression = {
    val expected = ToStringExpression(DynamicParameterExpression.withConstantDefault(context.parametersDao, metadata.name, requiredValue.getName, ConfigurationSetId, expectedValue)).noneable
    val actual = SingleSnapshotExtractExpression(SnapshotExpression(metricName).asSingle().mostRecent(), "value")

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The snapshot we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).single(),

        // The condition which, if true, we have an issue. Checked against the snapshots we've collected
        Equals(actual, expected).not
      ).withRootInfo(
        // Details of the alert itself
        getHeadline(),
        scopableStringFormatExpression("The configuration is not as expected:\nExpected: %s, Actual: %s.", expected, actual),
        ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText:_*)
      ).asCondition()
    ).withoutInfo()
  }
}
