#! META
name: f5-ls-l-var-core
description: Check if there is any core files
type: monitoring
monitoring_interval: 30 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
core-dumps:
    why: |
        Core dumps are created when a tmm process crashes and indicates that the system has an issue.
    how: |
        This alert logs into the F5 load balancer and looks in /var/core to see if there is any core files.
    without-indeni: |
        Login to your device with SSH and run "ls /var/core" and verify that there are no core files.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: Unknown

#! REMOTE::SSH
${nice-path} -n 15 /bin/ls --time-style=long-iso -l /var/core/

#! PARSER::AWK

BEGIN{

    iFile = 0

}

#lrwxrwxrwx 1 root root 25 2017-03-17 08:16 test.core.gz -> /shared/core/test.core.gz

/\.core\.gz$/{

    dateCreated = $6
    
    #2016-12-08
    split(dateCreated, dateArr, /-/)
    
    year = dateArr[1]
    month = dateArr[2]
    day = dateArr[3]
    
    timeCreated = $7
    
    #08:16
    split(timeCreated, timeArr, /:/)
    
    hour = timeArr[1]
    minute = timeArr[2]
    second = 0
    
    secondsSinceEpoch = datetime(year, month, day, hour, minute, second)
    
    iFile++
    coreFiles[iFile, "path"] = "/var/core" $8
    coreFiles[iFile, "created"] = secondsSinceEpoch

}

END {

    writeComplexMetricObjectArray("core-dumps", null, coreFiles)

}
