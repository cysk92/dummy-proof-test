#! META
name: f5-rest-mgmt-tm-vcmp-health-modules-provision
description: Extracts the vCMP guests provisioned modules
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    vsx: "true"
    shell: "bash"

#! COMMENTS
features-enabled:
    why: |
        This metric gives an administrator an easy way to get an overview of the vCMP guest configuration by listing the provisioned modules.
    how: |
        This script uses the F5 iControl REST API to retrieve the provisioned modules of vCMP guests.
    without-indeni: |
        This metric is available by logging into the device with SSH, entering TMSH and executing the command "show vcmp health module-provision".
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/vcmp/health/module-provision
protocol: HTTPS

#! PARSER::JSON

_metrics:
    - #Enabled features on the guests
        _groups:
            "$.entries.*.nestedStats.entries[?(@.provisionLevel.description != 'none')]":
                _tags:
                    "im.name":
                        _constant: "features-enabled"
                    "vs.name":
                        _value: "vcmpName.description"
                    "im.identity-tags":
                        _constant: "vs.name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Guest provisioned modules"
                _temp:
                    "name":
                        _value: "module.description"
        _transform:
            _value.complex:
                "name": |
                    {
                        #Let's define some friendly names for the known modules
                        moduleDictionary["mgmt"] = "Management"
                        moduleDictionary["cgnat"] = "Carrier Grade NAT"
                        moduleDictionary["ltm"] = "Local Traffic Manager"
                        moduleDictionary["asm"] = "Application Security Manager"
                        moduleDictionary["lc"] = "Link Controller"
                        moduleDictionary["apm"] = "Access Policy Manager"
                        moduleDictionary["avr"] = "Application Visibility and Reporting"
                        moduleDictionary["afm"] = "Advanced Firewall Module"
                        moduleDictionary["aam"] = "Application Acceleration Manager"
                        moduleDictionary["swg"] = "Secure Web Gateway"

                        #Look up the module in the moduleDictionary. If it does not exist, resort to the original value
                        if("${temp.name}" in moduleDictionary){
                            print "F5 " moduleDictionary["${temp.name}"]
                        } else {
                            print "F5 " "${temp.name}"
                        }
                    }
        _value: complex-array
