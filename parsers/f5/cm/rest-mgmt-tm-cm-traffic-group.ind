#! META
name: f5-rest-mgmt-tm-cm-traffic-group
description: Check if auto-failback-enabled has been set to true and set cluster-preemption if it has
type: monitoring
monitoring_interval: 60 minute 
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
cluster-preemption-enabled:
    why: |
        Preemption, or auto failback as F5 calls is, is a function in clustering which sets a primary member of the cluster to always strive to be the active member. The trouble with this is that if the active member that is set with preemption on has a critical failure and reboots, the cluster will fail over to the secondary and then immediately fail over back to the primary when it completes the reboot. This can result in another crash and the process would happen again and again in a loop. It is generally a good idea not to have the preemption feature enabled.
    how: |
        This script uses the F5 iControl API to retrieve the traffic group configuration to determine if auto failback is enabled or not.
    without-indeni: |
        An administrator would have to log in to his devices via the Web interface and verify that the auto failback option is not set. This could also be detected the hard way during an upgrade of a redundant pair if a recently upgraded node takes over prematurely.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/cm/traffic-group?$select=fullPath,autoFailbackEnabled
protocol: HTTPS

#! PARSER::JSON

_metrics:
    -
        _groups:
            "$.items[0:]":
                _temp:
                    #Count number of instances of autoFailbackEnabled where it's set to true
                    #This is used later on in the transform section
                    "autoFailbackCount":
                        _count: "[?(@.autoFailbackEnabled == 'true')]"
                _tags:
                    #Metric name
                    "im.name":
                        _constant: "cluster-preemption"
                    "im.dstype.displaytype":
                        _constant: "state"
                    #A tag called name with value of the name of the traffic group in     
                    "name":
                        _value: fullPath
        _transform:
            #Check the number of instances of autoFailBackEnabled that was found. If no instances was found set the value of the metric to 0
            _value.double: |
                {
                    if ("${temp.autoFailbackCount}" == 1) { print "1" } else { print "0" }
                }
