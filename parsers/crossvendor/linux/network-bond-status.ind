#! META
name: linux-network-bond-status
description: show the aggregator ID for the links in a bond
type: monitoring
monitoring_interval: 5 minutes
requires:
    or:
        -
            linux-based: true
        -
            linux-busybox: true

#! COMMENTS
interface-aggregator-id:
    why: |
        If the aggregator ID is different for the links in the same bond, that could indicate an incorrect configuration on the switch side.
    how: |
        List the aggregator ID for each interface and which bond they belong to.
    without-indeni: |
        An administrator could login and manually check the aggregator ID.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 ifconfig|grep bond | awk {'print $1'} | awk -F "." {'print $1'} | uniq | while read bond ; do echo $bond && cat /proc/net/bonding/$bond ;done

#! PARSER::AWK

# bond48
/bond/ {
	bond=$1
}

# Slave Interface: eth1-02
/Slave Interface/ {
	interfaceName=$3
}

# Aggregator ID: 1
/^Aggregator ID/ {
	aggregatorId=$3	

	nictags["interface-name"] = interfaceName
	nictags["bond-name"] = bond
	writeComplexMetricString("interface-aggregator-id", nictags, aggregatorId)
}
