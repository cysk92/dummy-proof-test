#! META
name: nexus-show-interface-transceiver-detail
description: fetch interface transceiver detail
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
hardware-element-status:
    why: |
       Capture different hardware element status information including Transmit and Receive optical level. The optical level readings of a port can indicate marginal light levels. While the port may be in up state, the link quality might be degraded. If the light level is outside of the expected range (reported by the device) an alert will be raised.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show interface transeiver detail" command. The output includes a table with the device's detailed light level information for all ports.
    without-indeni: |
       It is possible to poll this data through SNMP but additional external logic would be required to idenitify the value is our of range.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show interface transceiver detail | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__ 
_metrics:
    -
        _groups:
            # Tx Power Alarm: 0.0 if Tx Power is within warning range
            ${root}/TABLE_interface/ROW_interface[tx_pwr]:
                _temp:
                    tx_pwr:
                        _text: "tx_pwr"
                    low:
                        _text: "tx_pwr_warn_lo"
                    high:
                        _text: "tx_pwr_warn_hi"
                    interface:
                        _text: "interface"
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Optics"
                    "im.dstype.displayType":
                        _constant: "state"            
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.double:  |
                {
                    if ((${temp.tx_pwr} < ${temp.low}) ||  (${temp.tx_pwr} > ${temp.high})) 
                    { 
                        print "0.0" 
                    } 
                    else 
                    { 
                        print "1.0" 
                    }
                }
            _tags:
                "name": |
                    {
                        print "Optic Tx Power State ${temp.interface}"
                    }
    -
        _groups:
            # Rx Power Alarm: 0.0 if Rx Power is within warning range
            ${root}/TABLE_interface/ROW_interface[rx_pwr]:
                _temp:
                    rx_pwr:
                        _text: "rx_pwr"
                    low:
                        _text: "rx_pwr_warn_lo"
                    high:
                        _text: "rx_pwr_warn_hi"
                    interface:
                        _text: "interface"
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Optics"
                    "im.dstype.displayType":
                        _constant: "state"
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.double:  |
                {
                    if ((${temp.rx_pwr} < ${temp.low}) ||  (${temp.rx_pwr} > ${temp.high})) 
                    { 
                        print "0.0" 
                    } 
                    else 
                    { 
                        print "1.0" 
                    }
                }
            _tags:
                "name": |
                    {
                        print "Optic Rx Power State ${temp.interface}"
                    }
    -
        _groups:
            # Tx Power Alarm: 0.0 if Tx Power is within warning range
            ${root}/TABLE_interface/ROW_interface/TABLE_lane/ROW_lane[not(lane_number)]:
                _temp:
                    tx_pwr:
                        _text: "tx_pwr"
                    low:
                        _text: "tx_pwr_warn_lo"
                    high:
                        _text: "tx_pwr_warn_hi"
                    interface:
                        _text: ancestor::node()/interface
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Optics"
                    "im.dstype.displayType":
                        _constant: "state"            
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.double:  |
                {
                    if ((${temp.tx_pwr} < ${temp.low}) ||  (${temp.tx_pwr} > ${temp.high})) 
                    { 
                        print "0.0" 
                    } 
                    else 
                    { 
                        print "1.0" 
                    }
                }
            _tags:
                "name": |
                    {
                        print "Optic Tx Power State ${temp.interface}"
                    }
                "interface": |
                    {
                        print "${temp.interface}"
                    }
    -
        _groups:
            # Rx Power Alarm: 0.0 if Rx Power is within warning range
            ${root}/TABLE_interface/ROW_interface/TABLE_lane/ROW_lane[not(lane_number)]:
                _temp:
                    rx_pwr:
                        _text: "rx_pwr"
                    low:
                        _text: "rx_pwr_warn_lo"
                    high:
                        _text: "rx_pwr_warn_hi"
                    interface:
                        _text: ancestor::node()/interface
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Optics"
                    "im.dstype.displayType":
                        _constant: "state"
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.double:  |
                {
                    if ((${temp.rx_pwr} < ${temp.low}) ||  (${temp.rx_pwr} > ${temp.high})) 
                    { 
                        print "0.0" 
                    } 
                    else 
                    { 
                        print "1.0" 
                    }
                }
            _tags:
                "name": |
                    {
                        print "Optic Rx Power State ${temp.interface}"
                    }
                "interface": |
                    {
                        print "${temp.interface}"
                    }

    -
        _groups:
            # Tx Power Alarm: 0.0 if Tx Power is within warning range
            ${root}/TABLE_interface/ROW_interface/TABLE_lane/ROW_lane[lane_number]:
                _temp:
                    tx_pwr:
                        _text: "tx_pwr"
                    low:
                        _text: "tx_pwr_warn_lo"
                    high:
                        _text: "tx_pwr_warn_hi"
                    interface:
                        _text: ancestor::node()/interface
                    lane:
                        _text: "lane_number"
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Optics"
                    "im.dstype.displayType":
                        _constant: "state"            
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.double:  |
                {
                    if ((${temp.tx_pwr} < ${temp.low}) ||  (${temp.tx_pwr} > ${temp.high})) 
                    { 
                        print "0.0" 
                    } 
                    else 
                    { 
                        print "1.0" 
                    }
                }
            _tags:
                "name": |
                    {
                        print "Optic Tx Power State ${temp.interface} Lane ${temp.lane}"
                    }
                "interface": |
                    {
                        print "${temp.interface}:${temp.lane}"
                    }
    -
        _groups:
            # Rx Power Alarm: 0.0 if Rx Power is within warning range
            ${root}/TABLE_interface/ROW_interface/TABLE_lane/ROW_lane[lane_number]:
                _temp:
                    rx_pwr:
                        _text: "rx_pwr"
                    low:
                        _text: "rx_pwr_warn_lo"
                    high:
                        _text: "rx_pwr_warn_hi"
                    interface:
                        _text: ancestor::node()/interface
                    lane:
                        _text: "lane_number"
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Optics"
                    "im.dstype.displayType":
                        _constant: "state"
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.double:  |
                {
                    if ((${temp.rx_pwr} < ${temp.low}) ||  (${temp.rx_pwr} > ${temp.high})) 
                    { 
                        print "0.0" 
                    } 
                    else 
                    { 
                        print "1.0" 
                    }
                }
            _tags:
                "name": |
                    {
                        print "Optic Rx Power State ${temp.interface} Lane ${temp.lane}"
                    }
                "interface": |
                    {
                        print "${temp.interface}:${temp.lane}"
                    }

