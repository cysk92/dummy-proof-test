#! META
name: nexus-show-debug
description: Nexus show debug
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
debug-status:
    why: |
        Enabling debugging on a Cisco Nexus device enables the system administrator to get low level information about the system's operation. This functionality is often used for troubleshooting and it has a potential high impact on CPU utilization and system stability. It is highly undesirable to keep debugging enabled for extended periods of time.
    how: |
        This script logs into the Cisco Nexus switch using SSH and retrieves the status of running debugs. In normal operation there should not be any debugs enabled.
    without-indeni: |
        Enabled debug can be detected by logging to the device or by monitoring syslog message in case debug level logging has been enabled.
    can-with-snmp: false
    can-with-syslog: true

#! REMOTE::SSH
show debug

#! PARSER::AWK

/^debug / {
  sub(/^debug /, "")
  debug_tags["name"] = $0

  writeDoubleMetric("debug-status", debug_tags, "gauge", 300, "1.0")
}

/ debugging is on/ {
  sub(/ debugging is on.*$/, "")
  debug_tags["name"] = trim($0)

  writeDoubleMetric("debug-status", debug_tags, "gauge", 300, "1.0")
}
