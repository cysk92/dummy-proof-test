#! META
name: netobj_objects-mgmt.C
description: parse out information about objects from the database regarding management
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    role-management: true
    mds:
        neq: true

#! COMMENTS
known-devices:
    why: |
        To make adding more devices easy, a list of all devices managed by a management server that is monitored is retrieved.
    how: |
        By parsing the $FWDIR/database/netobj_objects.C file, all known devices that have a SIC connection are retrieved.
    without-indeni: |
        An administrator could enter the information manually, but risk that some systems are forgotten.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        An administrator could enter the information manually, but risk that some systems are forgotten.

#! REMOTE::SSH
${nice-path} -n 15 cat $FWDIR/database/netobj_objects.C

#! PARSER::AWK

function setNameSetData () {
	dataName = $1
	gsub(":","",dataName)

	# Set the data variable
	data=$2
	gsub("\\(","",data)
	gsub("\\)","",data)
	gsub("\"","",data)
}


BEGIN {
	sectionDepth=0
}

######## parsing C files ########

##############################
# The C files in check point consists of data, stored in sections. Each data has a name and a value. Each section has only a name.
# The sections are in hierarchies, and thus a section can contain multiple sub-section
# The section names can be in different formats, so we match against all of them
# Since it is important to know how far down in the hierarchie we are, we also store that. 
#############################

## Section name sections
## Here we will set the section name and on which level they are

# Name format 1
# :ike_p1 (
/:.+ \($/ {

	sectionName = $1
	
	# Removing junk
	gsub(":","",sectionName)
	sectionName = trim(sectionName)

	# Will count nr of tabs to see on which level we are
	sectionDepth=gsub(/\t/,"")
	sectionDepth++

	# Array to look up name
	sectionArray[sectionDepth] = sectionName
}


# Name example 2
# : (MyIntranet
# : (ReferenceObject
/: \(.+$/ {
	
	sectionName = $2
	
	# Removing junk
	gsub("\\(","",sectionName)
	sectionName = trim(sectionName)
	
	# Will count nr of tabs to see on which level we are
	sectionDepth=gsub(/\t/,"")
	sectionDepth++
	
	# Array to look up name
	sectionArray[sectionDepth] = sectionName
}



#Name example 3
# :ike_p1_dh_grp (ReferenceObject
# Any line with an ":" followed by any characters then a space, followed by a "(" but not ending with a ")"
/:.+ \(.[^)]*$/ {
	
	sectionName = $1
	
	# Removing junk
	gsub(":","",sectionName)
	sectionName = trim(sectionName)
	
	# Will count nr of tabs to see on which level we are
	sectionDepth=gsub(/\t/,"")
	sectionDepth++	

	# Array to look up name
	sectionArray[sectionDepth] = sectionName
}

#Name example 4
# (
/^\($/ {

	# Will count nr of tabs to see on which level we are
	sectionName = ""
	sectionDepth=gsub(/\t/,"")
	sectionDepth++	
}

# Decrease section depth by 1
# Match for a tab with a ")" after it and then line end
#	)
/\t\)$/ {
	# Tracks which level we are in the sections.
	# We encountered a ) and thus we are one level higher
	sectionDepth--
}

# Decrease section depth by 1
# Match for a tab with a ")" after it and then line end
# )
/^\)$/ {
	# Tracks which level we are in the sections.
	# We encountered a ) and thus we are one level higher
	sectionDepth--
}


# :ipaddr (10.11.2.21)
/:ipaddr/ {
	setNameSetData()

	# On depth 2 there should only be the "main" IP of the device
	if ( sectionDepth == 2 ) {
		# data = ip address, example: 1.1.1.1
		# [sectionArray[2]] = gateway name, example: lab-CP-GW5-R7730
		writeOk=0		
		# Now we will check all the data in the "gateways" array which hold info on all gateways that are member of a permanent tunnel, together with info
		# on which community name they are part of.
		iips++
		ips[iips] = sectionArray[2] "," data
	}
}

# :Name (lab-CP-MGMT-R7730)
/:Name/ {

	# set variables "data" and "dataName"
	setNameSetData()
	if (sectionArray[3] == "cluster_object") {
		# Array with cluster member hostname as key, and the cluster hostname as data
		clusters[sectionArray[2]] = data
	}
}

# :type (gateway)
/:type \(gateway\)/ {
	gateways[sectionArray[2]] = ""
}

# :masters (
/:masters/ {
	# if this is a locally managed gateway then the section "masters" exist
	if ( sectionDepth == 3 ) {
		masters[sectionArray[2]] = 1
	}
}


######## END tasks ########

END {
	
	# Put the IP adresses recorded in array "ips" into the "gateways" array
	for (ip in ips) {
		split(ips[ip], ipArr, ",") # GW,IP
		if (ipArr[1] in gateways) {
			gateways[ipArr[1]] = ipArr[2]
		}
	}
	
	for (id in gateways) {
		if ( masters[id] == 1) {
			# some VS object that we cannot poll can show IP as 0.0.0.0
			if (gateways[id] != "0.0.0.0") {
				knownDevices[id, "name"] = id
				knownDevices[id, "ip"] = gateways[id]
			}
		}
	}

	# Check if array knownDevices has data in it, and if so write metric
	if (arraylen(knownDevices)) {
		writeComplexMetricObjectArray("known-devices", null, knownDevices)
	}
}


