#! META
name: chkp-os-raid_diagnostic
description: Checks the health of a raid
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform
    asg:
        neq: true

#! COMMENTS
hardware-element-status:
    why: |
        If a RAID device is degraded, a possible loss of redundancy and disk performance may result in a severe issue.
    how: |
        Check the RAID status using the built in "raid_diagnostic" command.
    without-indeni: |
        An administrator could login and manually check the status.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 raid_diagnostic

#! PARSER::AWK

#VolumeID:0 RaidLevel: RAID-1 NumberOfDisks:0 RaidSize:1296385023GB State:DEGRADED Flags:ENABLED USING_INTERIM_RECOVERY_MODE
/State:/ {

	# The line can look very different. Need to search for the "State:" message.
	for (i = 1; i<=NF; i++) {
		if (match($i, /State:/)) {
			stateMessage = $i
			gsub(/State:/, "", stateMessage)
		}
	}
	
	# Allowing only "OPTIMAL" or "ONLINE" status, except for "MISSING" which needs to be allowed due to possible bug. sk104580
	if (stateMessage != "OPTIMAL" && stateMessage != "MISSING" && stateMessage != "ONLINE") {
		state = 0
	} else {
		state = 1
	}
	
	tags["name"] = "RAID " $1
	writeDoubleMetric("hardware-element-status", tags, "gauge", 300, state)
}
