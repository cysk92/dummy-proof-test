#! META
name: chkp-clish-show_snmp_agent
description: Show all SNMP settings
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: ipso

#! COMMENTS
snmp-enabled:
    skip-documentation: true

snmp-version:
    skip-documentation: true

snmp-contact:
    why: |
        If the wrong contact is specified in the SNMP settings, the network monitoring team might contact the wrong person or team when there is an issue.
    how: |
        Parse the GAiA configuration database in /config/active and retrieve the current configuration for SNMP.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SNMP information is only available from the command line interface and WebUI.

snmp-location:
    why: |
        The SNMP location is important, since it gives the administrator a fast and easy way to determine where it is located.
    how: |
        Parse the GAiA configuration database in /config/active and retrieve the current configuration for SNMP.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SNMP information is only available from the command line interface and WebUI.

snmp-communities:
    why: |
        If the default SNMP communities are configured, like "public" or "private" it could allow unauthorized clients to poll the device.
    how: |
        Parse the GAiA configuration database in /config/active and retrieve the current configuration for SNMP.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SNMP information is only available from the command line interface and WebUI.


snmp-traps-status:
    why: |
        SNMP configuration should be the same across cluster members. indeni retrieves SNMP configuration to compare between them.
    how: |
        Parse the GAiA configuration database in /config/active and retrieve the current configuration for SNMP.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SNMP information is only available from the command line interface and WebUI.

snmp-traps-reciever:
    why: |
        SNMP configuration should be the same across cluster members. indeni retrieves SNMP configuration to compare between them.
    how: |
        Parse the GAiA configuration database in /config/active and retrieve the current configuration for SNMP.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SNMP information is only available from the command line interface and WebUI.

snmp-users:
    why: |
        SNMP configuration should be the same across cluster members. indeni retrieves SNMP configuration to compare between them.
    how: |
        Parse the GAiA configuration database in /config/active and retrieve the current configuration for SNMP.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SNMP information is only available from the command line interface and WebUI.

unencrypted-snmp-configured:
    why: |
        If SNMP is not using version 3 only, this means that SNMP communication is not encrypted.
    how: |
        Parse the GAiA configuration database in /config/active and retrieve the current configuration for SNMP.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SNMP information is only available from the command line interface and WebUI.

#! REMOTE::SSH
${nice-path} -n 15 grep "snmp" /config/active

#! PARSER::AWK

BEGIN {
	snmpUnencrypted = "false"
}

#process:snmpd t
/process:snmpd t/ {
	enableStatus = "enabled"
}

#snmp:version v1/v2/v3
/snmp:version/ {
	snmpVersion = $2
	if (snmpVersion != "v3-Only") {
		# SNMPv3 only is not set, which means unencrypted SNMP communication.
		snmpUnencrypted = "true"
	}
}

#snmp:community:public t
/snmp:community|snmp:writecommunity/ {
	split($1, splitArr, /:/)
	iCommunity++
	communities[iCommunity, "community"] = splitArr[3]
	
	if (splitArr[2] == "writecommunity") {
		communities[iCommunity, "permissions"] = "read-write"
	} else if (splitArr[2] == "community") {
		communities[iCommunity, "permissions"] = "read-only"
	}
}

#snmp:syscontact Mr\ Cool
/snmp:syscontact/ {
	snmpContact = $0
	gsub(/snmp:syscontact/, "", snmpContact)
	# remove escape chars
	gsub(/\\ /, " ", snmpContact)
	snmpContact = trim(snmpContact)
}

#snmp:syslocation superduperlocation\ here
/snmp:syslocation/ {
	snmpLocation = $0
	gsub(/snmp:syslocation/, "", snmpLocation)
	# remove escape chars
	gsub(/\\ /, " ", snmpLocation)
	snmpLocation = trim(snmpLocation)
}

#snmp:traps:authorizationError on
# snmp:traps:NOKIA-IPSO-SYSTEM-MIB:systemtrapconfigurationsavechangeenable t
/snmp:traps:/ {
    sub(/(NOKIA-IPSO-SYSTEM-MIB|VRRP-MIB|rfc1157):/, "", $1) # In IPSO, there's the MIB name in the middle for some traps
	split($1,splitArr,/:/)
	trapName = trim(splitArr[3])
	if ($2 == "on" || $2 == "t") {
		trapStatus = "enabled"
	} else {
		trapStatus = "disabled"
	}
	
	iTrap++
	traps[iTrap, trapName] = trapStatus
}

#snmp:trap_rcv:1.1.1.1:version v2
#snmp:trap_rcv:1.1.1.1:community testcommunity
/snmp:trap_rcv:/ {
	split($1,splitArr,/:/)
	trapIp = splitArr[3]
	
	trapReciever[trapIp, "ip"] = trapIp
	if (trim(splitArr[4]) == "version") {
		trapReciever[trapIp, "version"] = $2
	} else if (trim(splitArr[4]) == "community") {
		trapReciever[trapIp, "community"] = $2
	}
}

#snmp:trapsPollingFrequency 100
/snmp:trapsPollingFrequency/ {
	iTrap++
	# Not supported in IPSO, ignored
	traps[iTrap, "pollingfrequency"] = $2
}

#snmp:v3:user:testuser:seclvl authPrivReq
/snmp:v3:user:.*seclvl/ {
	split($1,splitArr,/:/)
	user = splitArr[4]
	security = $2
	
	snmpUser[user, "username"] = user
	snmpUser[user, "security"] = security
	
	if (security == "authNoPriv") {
		# No encryption
		snmpUnencrypted = "true"
	}
}

#snmp:roUser:testuser t
#snmp:rwUser:testuser t
/(snmp:rwUser:|snmp:roUser:)/ {
	split($1,splitArr,/:/)
	user = splitArr[3]
	
	if (splitArr[2] == "rwUser") {
		snmpUser[user, "permissions"] = "ReadWrite"
	} else if (splitArr[2] == "roUser") {
		snmpUser[user, "permissions"] = "ReadOnly"
	}
}

END {
	# Only write SNMP data if SNMP is enabled.
	if (enableStatus != "enabled") {
		enableStatus = "disabled"
	}
	
	if (enableStatus == "enabled") {
		writeComplexMetricString("snmp-enabled", null, "true")
		writeComplexMetricObjectArray("snmp-communities", null, communities)
		writeComplexMetricObjectArray("snmp-traps-status", null, traps)
		writeComplexMetricObjectArray("snmp-traps-reciever", null, trapReciever)
		writeComplexMetricObjectArray("snmp-users", null, snmpUser)
		writeComplexMetricString("snmp-version", null, snmpVersion)
		writeComplexMetricStringWithLiveConfig("snmp-contact", null, snmpContact, "SNMP - Contact")
		writeComplexMetricStringWithLiveConfig("snmp-location", null, snmpLocation, "SNMP - Location")
		writeComplexMetricString("unencrypted-snmp-configured", null, snmpUnencrypted)
	} else {
		writeComplexMetricString("snmp-enabled", null, "false")
	}
}