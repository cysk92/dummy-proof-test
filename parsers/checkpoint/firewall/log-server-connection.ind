#! META
name: log-server-connected
description: Check if there is a connection to each configured log server
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    role-firewall: "true"
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform

#! COMMENTS
log-server-communicating:
    why: |
        It is useful for logs to be sent from devices to a central log storage. If the device has lost communication with the log server, it could begin logging locally instead. Some logs may be lost and the device's own storage may fill up.
    how: |
        By checking which connection the device currently has on port 257, and comparing that with the log servers configured it is possible to see if the device has a connection to the log server or not.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        There is no alert when a device has lost connection with the log server. Most of the cases it is only noticed when trying to view logs and discovering that logs from one or more devices is missing.

#! REMOTE::SSH
${nice-path} -n 15 netstat -anp | grep ":257" && grep ":MySICname" $CPDIR/registry/HKLM_registry.data && ${nice-path} -n 15 cat $FWDIR/database/netobj_objects.C

#! PARSER::AWK

############
# Why: If the device does not have a connection to the log server, then something is wrong.
# How: Compare the IP of the log server with a list of current connections from netstat.
# Detailed explanation:
# To determine if we are communicating with all log servers we need to:
# 1. Check which log servers we are communicating with right now, using netstat to see which IP adresses a connection on port 257 is established.
# 2. Check which primary log servers are configured
# To check which ones the device are communicating with, we use netstat and see which established connections we have on port 257
# This gives us the IP address of all log servers we are communicating with
# We then need to see which log servers are configured. One way is to use $FWDIR/conf/masters
# This file however only gives us the name of the log servers, not the IP.
# Another file to get information from is $FWDIR/database/netobj_objects.C
# Here we can see both which log servers are configured for the device, but also see what IP the log server has. 
# One problem however could be if NAT is involved, and the gateway is sending logs to an external IP that is then NAT to the MGMT.
# This should also be visible in the $FWDIR/database/netobj_objects.C file.
###########

function setNameSetData () {
	dataName = $1
	gsub(":", "", dataName)

	# Set the data variable
	data = $2
	gsub("\\(", "", data)
	gsub("\\)", "", data)
	gsub("\"", "", data)
}

BEGIN {
	sectionDepth = 0
}


#tcp        0      1 10.10.6.25:46576            1.1.1.2:257                 SYN_SENT    5510/fwd
#tcp        0      0 10.10.6.25:51047            10.10.6.10:257              ESTABLISHED 5510/fwd
/ESTABLISHED/ {
	# Creating list of log servers that has a connection (ESTABLISHED)
	logserverIp = $5
	gsub(/:257/, "", logserverIp)
	logserverConnArr[logserverIp] = 1
}

#:MySICname ("CN=lab-CP-GW1-1,O=lab-CP-MGMT-R7730-PRIMARY..smyyhc")
/MySICname/ {
	# Determining hostname from SIC certificate
	
	# extract hostname and remove junk
	split($2, hostnameArr, ",")
	gsub(/\(|\"|=|CN/, "", hostnameArr[1])
	
	localName = hostnameArr[1]
}


######## parsing C files ########


##############################
# The C files in check point consists of data, stored in sections. Each data has a name and a value. Each section has only a name.
# The sections are in hierarchies, and thus a section can contain multiple sub-section
# The section names can be in different formats, so we match against all of them
# Since it is important to know how far down in the hierarchie we are, we also store that. 
#############################

## Section name sections
## Here we will set the section name and on which level they are


# Name format 1
# :ike_p1 (
/:.+ \($/ {

	sectionName = $1
	
	# Removing junk
	gsub(":", "", sectionName)
	sectionName = trim(sectionName)

	# Will count nr of tabs to see on which level we are
	sectionDepth = gsub(/\t/, "")
	sectionDepth++

	# Array to look up name
	sectionArray[sectionDepth] = sectionName
}

# Name example 2
# : (MyIntranet
# : (ReferenceObject
/: \(.+$/ {
	
	sectionName = $2
	
	# Removing junk
	gsub("\\(", "", sectionName)
	sectionName = trim(sectionName)
	
	# Will count nr of tabs to see on which level we are
	sectionDepth = gsub(/\t/, "")
	sectionDepth++
	
	# Array to look up name
	sectionArray[sectionDepth] = sectionName
}

#Name example 3
# :ike_p1_dh_grp (ReferenceObject
# Any line with an ":" followed by any characters then a space, followed by a "(" but not ending with a ")"
/:.+ \(.[^)]*$/ {
	
	sectionName = $1
	
	# Removing junk
	gsub(":", "", sectionName)
	sectionName = trim(sectionName)
	
	# Will count nr of tabs to see on which level we are
	sectionDepth = gsub(/\t/, "")
	sectionDepth++	

	# Array to look up name
	sectionArray[sectionDepth] = sectionName
}

#Name example 4
# (
/^\($/ {

	sectionName = ""
	# Will count nr of tabs to see on which level we are
	sectionDepth = gsub(/\t/, "")
	sectionDepth++
}

# Decrease section depth by 1
# Match for a tab with a ")" after it and then line end
#	)
/\t\)$/ {
	# Tracks which level we are in the sections.
	# We encountered a ) and thus we are one level higher
	sectionDepth--
}

# Decrease section depth by 1
# Match for a tab with a ")" after it and then line end
# )
/^\)$/ {
	# Tracks which level we are in the sections.
	# We encountered a ) and thus we are one level higher
	sectionDepth--
}

#:ipaddr (10.11.2.21)
/:ipaddr/ {
	# Here we record the IPs of all check point objects into an array
	setNameSetData()

	# Get IPs from devices, both from interfacs and "main IP"
	if ( sectionArray[sectionDepth-1] == "interfaces" ||  sectionDepth == 2 ) {
		# data = ip address, example: 1.1.1.1
		# [sectionArray[2]] = gateway name, example: lab-CP-GW5-R7730
	
		# Now we will check all the data in the "gateways" array which hold info on all gateways that are member of a permanent tunnel, together with info
		# on which community name they are part of.
		iIPs++
		IPs[iIPs] = sectionArray[2] "," data # example: IPs[1] = GW1,1.1.1.1
	
		if ( sectionDepth == 2 ) {
			# This catches the "main IP", which could be useful if the VPN selection is based on that
			mainGwIp[sectionArray[2]] = data # example: mainGwIp["lab-CP-MGMT-LOGSRV1"] = 1.1.1.1
		}
	
	}
}

#:valid_ipaddr (1.1.1.1)
/:valid_ipaddr / {
	# If automatic NAT is used, the log server could have this IP as well
	
	# set variables "data" and "dataName"
	setNameSetData()
	
	if ( sectionDepth == 2 ) {
		iIPs++
		IPs[iIPs] = sectionArray[2] "," data # example: IPs[1] = GW1,1.1.1.1
	}
}

#:refname ("#_lab-CP-GW4")
/:refname / {
	# If the logserver is NATed behind a gateway, it shows here
	
	# set variables "data" and "dataName"
	setNameSetData()
	
	# Remove junk
	gsub(/#_/, "", data)
	
	natBehindGateway[sectionArray[2]] = data # example: natBehindGateway["lab-CP-MGMT-LOGSRV1"] = "lab-CP-GW1"
}

#:Name (lab-CP-MGMT-LOGSRV2)
/:Name/ {
	# If we find a check point device that has the SIC name found from the above "localName" we record which log server it uses.
	
	# data == logservername, sectionArray[2] == gatewayname
	setNameSetData()
	
	gatewayName = sectionArray[2]
	logServerName = data
	
	if ( sectionArray[sectionDepth-1] == "send_logs_to" && gatewayName == localName ) {
		logserverPrimaryConfArr[logServerName] = ""
	} else	if ( sectionArray[sectionDepth-1] == "backup_log_servers" && gatewayName == localName ) {
		logserverBackupConfArr[logServerName] = ""
	}
}

END {

	# Put the IP adresses recorded in the unsorted array "IPs" into the "gateways" array using the device name as key
	for (ip in IPs) {
		split(IPs[ip], ipArr, ",") # GW,IP
		if (ipArr[1] in gateways) {
			gateways[ipArr[1]] = gateways[ipArr[1]] "," ipArr[2]
		} else {
			gateways[ipArr[1]] = ipArr[2]
		}
	}
	
	# If a log server is NATed behind a gateway, add that gateways IPs to possible IPs the log server might hide behind
	for (id in natBehindGateway) { # example: natBehindGateway["lab-CP-MGMT-LOGSRV1"] = "lab-CP-GW1"
		gateways[id] = gateways[id] "," gateways[natBehindGateway[id]]
	}

	
	# Try to match the configured primary log servers against the list of log servers we have a connection to.
	for (id in logserverPrimaryConfArr) { # example: logserverPrimaryConfArr["lab-CP-MGMT-LOGSRV1"] = ""
		split(gateways[id], gatewaysSplitArr, ",")
		for (id2 in gatewaysSplitArr) { # example: gatewaysSplitArr[1] = 1.1.1.1
			if (gatewaysSplitArr[id2] in logserverConnArr) {
				# log server is up!
				# Set it to 1 and put the IP we know the connection uses as its IP.
				logserverPrimaryConfArr[id] = 1
				logserverPrimaryIPArr[id] = gatewaysSplitArr[id2]
				primaryLogServersUp++
			} else {
				# Not found to be up, so lets pretend it has the "main ip" configured for it
				logserverPrimaryIPArr[id] = mainGwIp[id]
			}
		}
	}
	
	# Are all active connections to log servers mapped to a device name? If so, then all other configured log servers are confirmed down
	if (arraylen(logserverConnArr) == primaryLogServersUp ) {
		for (id in logserverPrimaryConfArr) {
			if (logserverPrimaryConfArr[id] != 1) {
				logserverPrimaryConfArr[id] = 0
				stateForAll = 1 # If 1, then the script will not try the backup methods of determining which log servers are up
			}
		}
	}
	
	if (stateForAll != 1) {
		# Check if any primary log servers are not determined to be connected, and count how many
		for (id in logserverPrimaryConfArr) { # example: logserverPrimaryConfArr["lab-CP-MGMT-LOGSRV1"] = ""
			if (logserverPrimaryConfArr[id] != 1) {
				# There are configured log servers that we have not found are up. Counting how many
				logserversUnknown++
			}
		}
	}
	
	if (stateForAll != 1) {
		# Test if any of the active connections can be matched to a backup log server. That means that at least one primary log server is down.
		for (id2 in logserverBackupConfArr) { # example: logserverBackupConfArr["lab-CP-MGMT-LOGSRV2"] = ""
			split(gateways[id], gatewaysSplitArr, ",")
			for (id2 in gatewaysSplitArr) { # example: gatewaysSplitArr[1] = 1.1.1.1
				if (gatewaysSplitArr[id] in logserverConnArr) {
					# backup log server is up!
					# This means that at least the same amount of primary log servers has failed, as backup log servers are up
					backupLogServersUp++
				}
			}
		}
	}
	

	
	
	# BACKUP METHOD 1
	# If the number of configured log servers we cannot detemine state for, are the same as connections to log servers we can determine are backup log servers, then all log servers that has a unknown state should mean that they are down
	if (stateForAll != 1) {
		if (logserversUnknown == backupLogServersUp) {
			for (id in logserverPrimaryConfArr) {
				if (logserverPrimaryConfArr[id] != 1) {
					logserverPrimaryConfArr[id] = 0
					stateForAll = 1
				}
			}
		}
	}
	
	# BACKUP METHOD 2
	if (stateForAll != 1) {
		# If manual NAT is used, it could be impossible to map configured log servers to actual connections in netstat
		# To try to work around this the script checks of the number of connections to log servers are the same as the configured log servers, and then mark all configured log servers as up.
		# Or if no connections to log servers are present, mark all configured log servers as down.
		# This means that we will not know if a connection to a log server is to a primary or backup one, but will assume it is to the primary one and determine it as up
		# Using manual NAT for log servers is not best practice or the supported way in Check Point.
		# The downside of this method is that if a primary log server is down, and a backup log server has taken its place we might assume that all primary log servers are up.
		if ( arraylen(logserverConnArr) == arraylen(logserverPrimaryConfArr) ) { # If the number of successfull logserver connections are the same as the configured ones, they are all up
			for (id in logserverPrimaryConfArr) { # example: logserverPrimaryConfArr["lab-CP-MGMT-LOGSRV1"] = ""
				logserverConnArr[id] = 1
				stateForAll = 1
			}
		} else if (arraylen(logserverConnArr) == 0) { # If the number of successfull logserver connections are zero, they are all down
			for (id in logserverPrimaryConfArr) { # example: logserverPrimaryConfArr["lab-CP-MGMT-LOGSRV1"] = ""
				logserverConnArr[id] = 0
				stateForAll = 1
			}
		}
	}
	
	# For each log server that is  configured, we will check if its has a connection or not.
	# If manual NAT is used, then it could be impossible to map the configured log servers to the connections found in netstat.
	for (id in logserverPrimaryConfArr) { # example: logserverPrimaryConfArr["lab-CP-MGMT-LOGSRV1"] = ""

		t["name"] = id
		t["ip"] = logserverPrimaryIPArr[id]
		status = logserverPrimaryConfArr[id]
		if (status == "") { # If we have not determined the status to be up, set it to down. This is a last resort.
			status = 0
		}
		
		writeDoubleMetric("log-server-communicating", t, "gauge", 300, status)
	}
}