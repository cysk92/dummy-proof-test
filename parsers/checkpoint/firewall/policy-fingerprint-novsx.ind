#! META
name: policy-fingerprint
description: retrive policy name and unique identifier
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform
    vsx:
        neq: true
    role-firewall: "true"

#! COMMENTS
policy-installed-fingerprint:
    why: |
        If all members of a cluster do not have the same security policy installed, unexpected issues can arise after a failover.
    how: |
        An MD5 hash is calculated along with the policy name.
    without-indeni: |
        An administrator could login and manually check which policy is installed, and when it was installed, comparing between all cluster members.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 fw stat && ${nice-path} -n 15 md5sum $FWDIR/state/local/FW1/local.str

#! PARSER::AWK

# localhost InitialPolicy 12Feb2017 19:13:38 :  [>eth0] [<eth0]
/^localhost/ {
	policyName=$2
	fingerprint = policyName
}

# 16f7b38c2a9e2f96a6faf3000f2050ff  /opt/CPsuite-R77/fw1/state/local/FW1/local.str
#/[a-f0-9]{32}/ {
/local\.str/ {
	if (fingerprint == "-") {
		fingerprint = ""
	} else if ($1 ~ /[a-f0-9]{32}/) {
		fingerprint = fingerprint " " $1
	}

	writeComplexMetricString("policy-installed-fingerprint", null, fingerprint)
}
