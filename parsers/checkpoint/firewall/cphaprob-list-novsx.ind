#! META
name: cphaprob_list
description: run "cphaprob list" to find pnotes in "problem"
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    high-availability: "true"
    vsx:
        neq: true
    role-firewall: true

#! COMMENTS
clusterxl-pnote-state:
    why: |
        If a device in a cluster discovers a problem with itself, it will be because one pre-defined check "pnote" ("problem notification") has failed. It is interesting to know which pnote failed, to begin an investigation into why it happened.
    how: |
        By using the Check Point built-in command "cphaprob list" the pnotes are retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing pnotes is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 cphaprob list

#! PARSER::AWK

BEGIN {
    devicename=""
}

# Device Name: admin_down
/Device Name/ {
    devicename = trim(substr($0, index($0, ":")+1))
}

# Current state: problem (non-blocking)
# Current state: problem
/Current state/ {
	state_desc = $3
	pnotestatustag["name"] = devicename
	isup = 0
	if (state_desc != "problem") {
		isup = 1
	}
	writeDoubleMetricWithLiveConfig("clusterxl-pnote-state", pnotestatustag, "gauge", "60", isup, "ClusterXL Devices", "state", "name")
}
