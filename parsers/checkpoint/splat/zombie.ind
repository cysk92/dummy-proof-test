#! META
name: chkp-secureplatform-zombie
description: displays list of zombie processes
type: monitoring
monitoring_interval: 10 minute
requires:
    vendor: checkpoint
    os.name: secureplatform

#! COMMENTS
tasks-zombies:
    why: |
        A zombie process is a child process that has died. The parent process has however not read the exit status and ended the child process. This means that it continues to use memory. Zombie processes are often a result of poorly written software. Since they still use memory a lot of zombie processes could cause issues.
    how: |
        Using the built in "ps" command, the number of zombie processes is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing amount of zombie processes is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 ps axo pid=,stat= | awk '$2~/^Z/ { print }' |grep -c .

#! PARSER::AWK

# 1
/[0-9]/ {
	zombies = $1
	writeDoubleMetric("tasks-zombies", null, "gauge", "600", zombies)
}