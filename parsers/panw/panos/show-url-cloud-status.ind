#! META
name: panos-show-url-cloud-status
description: get the status of the URL filtering cloud
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
url-filtering-cloud-connected:
    why: |
        For URL filtering to work correctly on a Palo Alto Networks firewall, a connection to Palo Alto Network's cloud is required (see https://www.paloaltonetworks.com/products/secure-the-network/subscriptions/url-filtering-pandb ). If the cloud connection is lost, the filtering behavior may be impacted.
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current status of the connectivity to the URL cloud  (the equivalent of running "show url-cloud status" in CLI).
    without-indeni: |
        An administrator will find out the communication with the URL cloud is lost only when certain features of the URL filtering doesn't work the way they expect it to. 
    can-with-snmp: true
    can-with-syslog: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><url-cloud><status></status></url-cloud></show>&key=${api-key}
protocol: HTTPS

#! PARSER::AWK
BEGIN {
	connected = ""
}

# NOTE:
# The output of this API call may come as a CDATA (older versions) or XML (newer versions)

# Cloud connection :                 not connected
# OR
# Cloud connection :                 connected
/Cloud connection/ {
	if ($(NF-1) == "not") {
		connected = 0
	} else {
		connected = 1
	}
}

# <cloud-connection>connected</cloud-connection>
/cloud-connection/ {
	if ($0 ~ />connected</) {
		connected = 1
	} else {
		connected = 0
	}
}

END {
	if (connected != "") {
		writeDoubleMetricWithLiveConfig("url-filtering-cloud-connected", null, "gauge",3600, connected, "URL Cloud Connected?", "boolean", "")
	}
}
