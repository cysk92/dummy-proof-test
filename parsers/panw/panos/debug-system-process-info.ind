#! META
name: panos-debug-system-process-info
description: grab list of processes
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
process-state:
    why: |
        Each device has certain executable processes which are critical to the stable operation of it. Within Palo Alto Networks firewalls, these processes are responsible for the management layer (mgmtsrvr), certain services (like dhcp and snmp), VPN (like ikemgr and keymgr) and many other functions. A process being down may indicate a critical failure.
    how: |
        This script logs into the Palo Alto Networks firewall through SSH and retrieves the status of running processes. It then compares the list of running processes to a known list of processes that are critical and checks to see they are all up. Those that are down are flagged as such.
    without-indeni: |
        An administrator would need to write a script to poll their firewalls for the data. The other option is to pull this data during an outage.
    can-with-snmp: false
    can-with-syslog: false
process-cpu:
    skip-documentation: true
process-memory:
    skip-documentation: true

#! REMOTE::SSH
debug system process-info

#! PARSER::AWK
BEGIN {
	critprocesses["mgmtsrvr"] = "Management Plane server for configuring policy device settings etc."
	critprocesses["all_task"] = "N/A"
	critprocesses["authd"] = "Handles authentication functionality"
	critprocesses["brdagent"] = "Mother board agent hardware monitor"
	critprocesses["chasd"] = "N/A"     
	critprocesses["comm"] = "N/A"
	critprocesses["crypto"] = "N/A"
	critprocesses["dagger"] = "N/A"
	critprocesses["devsrvr"] = "Device server"
	critprocesses["dha"] = "N/A"
	critprocesses["dhcp"] = "DHCP service"
	critprocesses["dnsproxy"] = "DNS Proxy"
	critprocesses["ehmon"] = "Hardware monitor"
	critprocesses["ha-sshd"] = "High-availability SSH connection"
	critprocesses["ha_agent"] = "High-availability agent"
	critprocesses["ikemgr"] = "IKE Manager for IPsec tunnels"
	critprocesses["keymgr"] = "Key Manager for IPsec tunnels"
	critprocesses["l3svc"] = "Captive portal server"
	critprocesses["logrcvr"] = "Log receiver"
	critprocesses["masterd"] = "Responsible for the health of other critical processes"
	critprocesses["monitor"] = "N/A"
	critprocesses["monitor-dp"] = "Dataplane monitoring process"
	critprocesses["mprelay"] = "Responsible for communication between the management plane and the data plane"
	critprocesses["rasmgr"] = "Remote access service - GlobalProtect"
	critprocesses["routed"] = "Dynamic routing daemon"
	critprocesses["satd"] = "N/A"
	critprocesses["snmpd"] = "SNMP trap sender and agent"
	critprocesses["sshd"] = "SSH server"
	critprocesses["sslmgr"] = "SSL certificate manager"
	critprocesses["sslvpn"] = "SSL VPN service"
	critprocesses["sysd"] = "N/A"
	critprocesses["sysdagent"] = "N/A"
	critprocesses["useridd"] = "User-ID agent daemon"
	critprocesses["varrcvr"] = "WildFire registration server"
	critprocesses["websrvr"] = "Web interface server"
	# Initialize the foundprocess array
	for (key in critprocesses) { foundprocess[key] = "0.0" }
}

# Name                   PID      CPU%  FDs Open   Virt Mem     Res Mem      State     
# all_task               2518     4     9          1978716      1914604      S         
/ S \s*$/ {
	if (foundprocess[$1] == "0.0") { foundprocess[$1] = "1.0" }
    pid = $2
	cpu = $3
	virtmem = $5
	resmem = $6
	state = $NF
	processname = $1
	command = $1

    pstags["name"] = pid
    pstags["process-name"] = processname
    pstags["command"] = command

    writeDoubleMetricWithLiveConfig("process-cpu", pstags, "gauge", "600", cpu, "Processes (CPU)", "percentage", "name|process-name")
    #        writeDoubleMetricWithLiveConfig("process-memory", pstags, "gauge", "600", memory, "Processes (Memory)", "percentage", "name|process-name|command")
}
END {
	for (process in foundprocess) {
		# By running the "if (foundprocess[$1] == "0.0")" further above we create empty entries in foundprocess, so
		# we must skip those
		if (foundprocess[process] == "0.0" || foundprocess[process] == "1.0") {
			critpstags["name"] = process
			critpstags["description"] = critprocesses[process]
			writeDoubleMetric("process-state", critpstags, "gauge", "600", foundprocess[process])
		}
	}
}
