#! META
name: panos-show-ntp
description: fetch the status of the NTP sync
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
ntp-server-state:
    why: |
        Having a correctly set clock on a Palo Alto Networks firewall is important ( see https://www.stigviewer.com/stig/palo_alto_networks_ndm/2016-06-30/finding/V-62755 ). The best way to ensure the clock is correctly set is to use NTP. Since sometimes the firewall may have issues communicating with the NTP server, tracking the status of the NTP synchornization is required.
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current status of the NTP servers (the equivalent of running "show ntp" in CLI).
    without-indeni: |
        An administrator could write a script to leverage the Palo Alto Networks API to collect this data periodically and alert appropriately. The system logs also include updates regarding successful or unsuccesful NTP synchronization.
    can-with-snmp: false
    can-with-syslog: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><ntp></ntp></show>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result
_metrics:
    -
        _groups:
            ${root}/*:
                _temp:
                    status: 
                        _text: "status"
                _tags:
                    name:
                        _text: "name"
                    "im.name":
                        _constant: "ntp-server-state"
                    "live-config":
                       _constant: "true"
                    "display-name":
                        _constant: "NTP Servers - State"
                    "im.dstype.displayType":
                        _constant: "state"
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.double: |
                {
                    if ("${temp.status}" == "synched" || "${temp.status}" == "available") {print "1.0"} else {print "0.0"}
                }
