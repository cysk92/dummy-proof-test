#! META
name: panos-show-system-files
description: check to see what system files are saved 
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
core-dumps:
    why: |
        Knowing if a critical process created a core dump is important. A core dump usually occurs when a process crashes, indicating an unexpected behavior. This can result in service disruption without a clear reason why - the user experience would be that sometimes "things don't work".
    how: |
        This script logs into the Palo Alto Networks firewall through SSH and retrieves the list of system files on the device. In that list, it searches for the core dumps.
    without-indeni: |
        A user would wait for "weird experiences" and open a support ticket with TAC. The TAC support engineer may direct the user to look at the core dumps.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show system files

#! PARSER::AWK
BEGIN {
}

# /var/cores/:
/\/.*\:$/ {
	path = $1
	sub(/:/, "", path)
	if (path !~ /\/$/) {
		path = path "/"
	}
}

# -rw-rw-rw- 1 root root 3.1K Jun  4  2015 dha_6.1.2_0.info
# IMPORTANT: We ignore directories (no "d" in the regex below)
/^[rwx\-]+/ {
	if (path ~ /core/) {
		ifile++
		year=$(NF-1)
		# The "year" column might be a timestamp (like 00:12) which means the year
		# is actually "this" year.
		if (year !~ /\d\d\d\d/) {
			year=2016 # temporary hack, we don't have how to get "this year" yet
		}
		month=$(NF-3)
		day=$(NF-2)

		files[ifile, "path"]=path $NF
		files[ifile, "created"]=date(year, parseMonthThreeLetter(month), day)
	}
}
END {
	# Write complex metric
    writeComplexMetricObjectArray("core-dumps", null, files)
}
