#! META
name: panos-debug-dataplane-monitor-detail-show
description: grab the debug status of debug database monitor detail
type: monitoring
monitoring_interval: 1 hour
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall



#! COMMENTS
debug-status:
    why: |
         When an administrator enables debugging on a device it is possible that device will experience an increase in CPU utilization. To avoid forgetting the debug on, it is important to track which debugs are still enabled and alert accordingly.
    how: |
        This script logs into the Palo Alto Networks firewall through SSH and retrieves the status of the debug (on or off).
    without-indeni: |
        Generally, administrators try to remember to disable a debug after enabling it. Often times this doesn't happen and is left un-detected. A restart of the device will usually reset the debug status, but only happens every few months.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
debug dataplane monitor detail show

#! PARSER::AWK
BEGIN {
	state = 0
}
/e/{
	if ($1 == "True")  {
		state = 1
	}
}

END {
     debugtags["name"] = "debug.dataplane.monitor.detail"
     writeDoubleMetric("debug-status", debugtags, "gauge", 3600, state)
}

